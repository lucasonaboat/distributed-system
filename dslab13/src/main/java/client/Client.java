package client;

import cli.Shell;
import util.ComponentFactory;
import util.Config;

public class Client {
	
	public static void main(String[] args) {
		Config config = new Config("client");
		Shell shell = new Shell("client", System.out, System.in);
		
		ComponentFactory comp = new ComponentFactory();
		try 
		{
			comp.startClient(config, shell);
		} 
		catch (Exception e) {
		}
		
	}

	

}
