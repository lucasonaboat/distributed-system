package client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import message.Response;
import message.request.AesChannelRequest;
import message.request.BuyRequest;
import message.request.ChannelRequest;
import message.request.CreditsRequest;
import message.request.DownloadFileRequest;
import message.request.DownloadTicketRequest;
import message.request.ListRequest;
import message.request.LoginRequest;
import message.request.LogoutRequest;
import message.request.UploadRequest;
import message.response.BuyResponse;
import message.response.CreditsResponse;
import message.response.DownloadFileResponse;
import message.response.DownloadTicketResponse;
import message.response.ListResponse;
import message.response.LoginResponse;
import message.response.LoginResponse.Type;
import message.response.MessageResponse;
import message.response.ProxyAuthenticationResponse;
import model.channel.AesEncryptionChannel;
import model.channel.Channel;
import model.channel.SimpleTcpChannel;
import model.channel.TcpChannel;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Base64;

import cli.Command;
import cli.Shell;
import convert.ConversionService;

public class ClientCli implements IClientCli {

	private Socket tcpSocket;
	private String directory;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private boolean loggedIn = false;
	private Shell shell;
	private TcpChannel encryptChannel;
	private TcpChannel decryptChannel;
	private ConversionService convertor;
	private String username;
	private byte[] secretKey;
	private byte[] ivParArr;
	private String noKeyFound = "There was no key found";

	public ClientCli(Socket tcpSocket, String directory, Shell shell) {
		this.directory = directory;
		this.tcpSocket = tcpSocket;
		this.shell = shell;

		try {

			encryptChannel = new SimpleTcpChannel();
			encryptChannel = new AesEncryptionChannel(encryptChannel);

			decryptChannel = new SimpleTcpChannel();
			decryptChannel = new AesEncryptionChannel(decryptChannel);

			in = new ObjectInputStream(tcpSocket.getInputStream());
			out = new ObjectOutputStream(tcpSocket.getOutputStream());

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public LoginResponse setUpAesChannel(String usernameLogin) {
		username = usernameLogin;
		TcpChannel loginTest = new SimpleTcpChannel();
		loginTest = new Channel(loginTest);
		// prepare first message
		byte[] loginByteArr = loginTest.clientLoginRequest(username);

		// error checking for no key found
		String loginString = new String(loginByteArr);
		if (loginString.equals(noKeyFound)) {
			return new LoginResponse(Type.WRONG_CREDENTIALS);
		}

		// send the first message
		ChannelRequest firstMessage = new ChannelRequest(loginByteArr);

		try {
			out.writeObject(firstMessage);
			out.flush();
			System.out.println("line1");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Problem sending the 1st message to proxy");
		}

		try {
			System.out.println("Receiving object from server: ");
			Object fromServer = in.readObject();

			System.out.println("object from server: " + fromServer.toString());
			System.out
					.println("Receiving auth from Server. Message is from class: "
							+ fromServer.getClass());
			if (fromServer instanceof ProxyAuthenticationResponse) {
				System.out
						.println("Receiving authentication response from server");
				byte[] byteArrFromServer = ((ProxyAuthenticationResponse) fromServer)
						.getAuthenticationResponse();
				System.out.println("Byte array from server is: "
						+ byteArrFromServer);
				if (byteArrFromServer == null) {
					System.out.println("Second message from server is null");
				}

				// check if the server returned with a no key message
				// if so then the final message won't be sent

				String fromServerString = new String(byteArrFromServer);
				if (fromServerString.equals(noKeyFound)) {
					return new LoginResponse(Type.WRONG_CREDENTIALS);
				}
				try {
					byte[] sendFinalAuthChannel = clientSendFinalMessage(
							byteArrFromServer, username);

					System.out
							.println("Sending the final message from the client");
					System.out.println("Sending : " + sendFinalAuthChannel);

					AesChannelRequest aesChannelReq = new AesChannelRequest(
							sendFinalAuthChannel);
					System.out.println("Message sending is: " + aesChannelReq);
					out.writeObject(aesChannelReq);
					out.flush();

					return new LoginResponse(Type.SUCCESS);
				} catch (IOException e) {
					System.out
							.println("Problem sending the final message to the proxy");
					e.printStackTrace();
				}

			}

		} catch (IOException e) {
			System.out.println("Problem receiving the2nd message from proxy");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new LoginResponse(Type.WRONG_CREDENTIALS);
	}

	@Override
	@Command
	public LoginResponse login(String username, String password) {
		System.out.println("Setting up the secure channel");
		// initial login request to get the secure channel

		LoginResponse setUpAesChannelResponse = setUpAesChannel(username);

		LoginResponse responseSuccess = new LoginResponse(Type.SUCCESS);
		LoginResponse responseFailure = new LoginResponse(
				Type.WRONG_CREDENTIALS);

		// set up the secure channel.
		if (setUpAesChannelResponse.equals(responseFailure)) {
			return responseFailure;
		}

		LoginRequest requestLogin = new LoginRequest(username, password);

		byte[] requestToBytes = objectToBytes(requestLogin);
		System.out.println("Request login to bytes " + requestToBytes);

		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}

		byte[] request64 = Base64.encode(requestToBytes);
		System.out.println("Request base 64: " + request64);

		System.out.println("Set up cipher");

		System.out.println("Test secrets: " + secretKey + " " + ivParArr);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			System.out.println("Set up sealed object");

			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return responseFailure;
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
		} catch (IOException e) {
			System.err.println("Connection error");
		}
		// LoginResponse input = null;
		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha = new SimpleTcpChannel();
			cha = new AesEncryptionChannel(cha);
			Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			Object builderObject = bytesToObject(sealedDecode64);
			LoginResponse loginResp = (LoginResponse) builderObject;

			if (loginResp.getType().equals(Type.SUCCESS)) {
				loggedIn = true;
				return responseSuccess;
			}
		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			System.err.println("Connection error");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}
		return responseFailure;

	}

	public synchronized byte[] objectToBytes(Object message) {
		try {
			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			objectOut.writeObject(message);
			byte[] sealedBytes = byteArrayOs.toByteArray();
			objectOut.close();
			byteArrayOs.close();

			return sealedBytes;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	@Command
	public Response credits() {
		CreditsRequest request = new CreditsRequest();
		LoginResponse responseFailure = new LoginResponse(
				Type.WRONG_CREDENTIALS);

		if (loggedIn != true) {
			return new MessageResponse("User not logged in");
		}
		byte[] requestToBytes = objectToBytes(request);
		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}
		byte[] request64 = Base64.encode(requestToBytes);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return responseFailure;
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
		} catch (IOException e) {
			System.err.println("Connection error");
		}
		// LoginResponse input = null;
		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha = new SimpleTcpChannel();
			cha = new AesEncryptionChannel(cha);
			Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			Object builderObject = bytesToObject(sealedDecode64);
			CreditsResponse creditsResp = (CreditsResponse) builderObject;
			return creditsResp;

		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			System.err.println("Connection error");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}
		return responseFailure;
	}

	@Override
	@Command
	public Response buy(long credits) {
		if (loggedIn != true) {
			return new MessageResponse("User not logged in");
		}
		LoginResponse responseFailure = new LoginResponse(
				Type.WRONG_CREDENTIALS);
		BuyRequest request = new BuyRequest(credits);

		byte[] requestToBytes = objectToBytes(request);
		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}
		byte[] request64 = Base64.encode(requestToBytes);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return responseFailure;
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
		} catch (IOException e) {
			return new MessageResponse("Failed to send request");
		}

		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha = new SimpleTcpChannel();
			cha = new AesEncryptionChannel(cha);
			Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			Object builderObject = bytesToObject(sealedDecode64);
			BuyResponse buyResp = (BuyResponse) builderObject;
			return buyResp;

		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			System.err.println("Connection error");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}

		return new MessageResponse("Unable to respond to the credits response");
	}

	@Override
	@Command
	public Response list() {
		ListRequest request = new ListRequest();
		byte[] requestToBytes = objectToBytes(request);
		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}
		LoginResponse responseFailure = new LoginResponse(
				Type.WRONG_CREDENTIALS);

		if (loggedIn != true) {
			return new MessageResponse("User not logged in");
		}
		byte[] request64 = Base64.encode(requestToBytes);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return responseFailure;
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
		} catch (IOException e) {
			return new MessageResponse("Failed to send request");
		}

		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha = new SimpleTcpChannel();
			cha = new AesEncryptionChannel(cha);
			Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			Object builderObject = bytesToObject(sealedDecode64);
			ListResponse listResp = (ListResponse) builderObject;
			return listResp;

		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			return new MessageResponse("Failed to retrieve response");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}
		return new MessageResponse("Failed to send request");
	}

	@Override
	@Command
	public Response download(String filename) {
		DownloadTicketRequest request = new DownloadTicketRequest(filename);
		byte[] requestToBytes = objectToBytes(request);
		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}
		LoginResponse responseFailure = new LoginResponse(
				Type.WRONG_CREDENTIALS);

		byte[] request64 = Base64.encode(requestToBytes);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return responseFailure;
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
		} catch (IOException e) {
			return new MessageResponse("Failed to send request");
		}
		Object input1 = null;
		DownloadTicketResponse input = null;
		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha1 = new SimpleTcpChannel();
			cha1 = new AesEncryptionChannel(cha1);
			Cipher decryptCipher = cha1.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			input1 = bytesToObject(sealedDecode64);
			if (input1 instanceof MessageResponse) {
				MessageResponse msg = (MessageResponse)input1;
				return msg;
			} else {
				input = (DownloadTicketResponse) input1;
			}
		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			return new MessageResponse("Failed to retrieve ticket");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}

		// unencrypted messages to and from the fileserver

		DownloadFileRequest dl = new DownloadFileRequest(input.getTicket());

		int port = input.getTicket().getPort();
		InetAddress addr = input.getTicket().getAddress();

		Socket fsSocket = null;
		DownloadFileResponse fileResponse = null;
		try {
			fsSocket = new Socket(addr, port);
			ObjectOutputStream fsOut = new ObjectOutputStream(
					fsSocket.getOutputStream());
			ObjectInputStream fsIn = new ObjectInputStream(
					fsSocket.getInputStream());
			fsOut.writeObject(dl);
			fsOut.flush();
			fileResponse = (DownloadFileResponse) fsIn.readObject();
			FileWriter fw = new FileWriter(directory + "/" + filename, false);
			fw.write(new String(fileResponse.getContent()));
			fw.flush();
			fw.close();
			fsOut.close();
			fsIn.close();

		} catch (IOException e) {
			return new MessageResponse("Failed download file");
		} catch (ClassNotFoundException e) {
		}

		try {
			fsSocket.close();
		} catch (IOException e) {

		}

		return fileResponse;
	}

	@Override
	@Command
	public MessageResponse upload(String filename) {

		String filePath = directory + "/" + filename;
		File up = new File(filePath);
		MessageResponse input = null;
		if (up.exists()) {
			String content = "";
			FileReader fr;
			char[] contentChars = new char[(int) up.length()];
			try {
				fr = new FileReader(up);
				BufferedReader br = new BufferedReader(fr);

				br.read(contentChars);
				br.close();
				fr.close();
			} catch (FileNotFoundException e) {
				return new MessageResponse("File not found");
			} catch (IOException e) {
			}
			content = new String(contentChars);
			byte[] byteContent = content.getBytes();

			UploadRequest request = new UploadRequest(filename, 0, byteContent);
			byte[] requestToBytes = objectToBytes(request);
			if (requestToBytes == null) {
				System.out.println("Request login is null");
			}
			LoginResponse responseFailure = new LoginResponse(
					Type.WRONG_CREDENTIALS);

			if (loggedIn != true) {
				return new MessageResponse("User not logged in");
			}
			byte[] request64 = Base64.encode(requestToBytes);
			Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
			SealedObject requestSealed = null;
			try {
				requestSealed = new SealedObject(request64, cipher);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			}
			if (requestSealed == null) {
				System.out.println("Sealed object is null");
				return new MessageResponse("Could not upload file");
			}

			try {
				out.writeObject(requestSealed);
				out.flush();
			} catch (IOException e) {
				return new MessageResponse("Failed to send request");
			}

			try {
				SealedObject sealed = (SealedObject) in.readObject();
				TcpChannel cha = new SimpleTcpChannel();
				cha = new AesEncryptionChannel(cha);
				Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
				byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
				byte[] sealedDecode64 = Base64.decode(sealedBytes);
				Object builderObject = bytesToObject(sealedDecode64);
				MessageResponse msgResp = (MessageResponse) builderObject;
				return msgResp;

			} catch (ClassNotFoundException e) {

			} catch (IOException e) {
				return new MessageResponse("Failed to retrieve response");
			} catch (IllegalBlockSizeException e) {
				System.err.println("Connection error");
			} catch (BadPaddingException e) {
				System.err.println("Connection error");
			}

			return new MessageResponse("uploading " + filename + " succeeded!");
		}
		return new MessageResponse("uploading " + filename
				+ " failed, file not found!");
	}

	@Override
	@Command
	public MessageResponse logout() {
		LogoutRequest request = new LogoutRequest();
		loggedIn = false;
		byte[] requestToBytes = objectToBytes(request);
		if (requestToBytes == null) {
			System.out.println("Request login is null");
		}
		byte[] request64 = Base64.encode(requestToBytes);
		Cipher cipher = encryptChannel.encryptCipher(secretKey, ivParArr);
		SealedObject requestSealed = null;
		try {
			requestSealed = new SealedObject(request64, cipher);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		if (requestSealed == null) {
			System.out.println("Sealed object is null");
			return new MessageResponse("Could not log out");
		}

		try {
			out.writeObject(requestSealed);
			out.flush();
			
		} catch (IOException e) {
			return new MessageResponse("Failed to send request");
		}
		
		try {
			SealedObject sealed = (SealedObject) in.readObject();
			TcpChannel cha = new SimpleTcpChannel();
			cha = new AesEncryptionChannel(cha);
			Cipher decryptCipher = cha.decryptCipher(secretKey, ivParArr);
			byte[] sealedBytes = (byte[]) sealed.getObject(decryptCipher);
			byte[] sealedDecode64 = Base64.decode(sealedBytes);
			Object builderObject = bytesToObject(sealedDecode64);
			MessageResponse logoutResp = (MessageResponse) builderObject;
			return logoutResp;

		} catch (ClassNotFoundException e) {

		} catch (IOException e) {
			return new MessageResponse("Failed to retrieve response");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Connection error");
		} catch (BadPaddingException e) {
			System.err.println("Connection error");
		}
		
		return new MessageResponse("Failed to retrieve response");
		
	}

	@Override
	@Command("!exit")
	public MessageResponse exit() {
		System.out.println("Trying to exit");
		if (loggedIn) {
			System.out.println("Trying to logout");
			MessageResponse logoutRes = logout();
			System.out.println(logoutRes);
			
		}
		else{
			System.out.println("Person is not logged in..");
		}
		try {
			if (tcpSocket != null) {
				
				System.out.println("Closing streams and socket");
				in.close();
				out.close();
				tcpSocket.close();
				
			}
			else{
				System.out.println("the tcp socket is not null");
			}
		} catch (IOException e) {
		}
		try {
			System.in.close();
		} catch (IOException e) {
		}
		shell.close();
		return new MessageResponse("Shutting down client now");
	}

	private class PasswordFinderImpl implements PasswordFinder {

		@Override
		public char[] getPassword() {
			// TODO Auto-generated method stub
			System.out.println("getting password");
//			try{
//				return new BufferedReader(new InputStreamReader(System.in)).readLine().toCharArray();
//			}catch(IOException e){
//				System.out.println("Could not determine passphrase");
//			}
//			return null;
			
			if (username.equals("alice")) {
				return "12345".toCharArray();
			} else if (username.equals("bill")) {
				return "23456".toCharArray();
			}
			{
				return "11111".toCharArray();
			}
			
		}

	}

	public byte[] clientSendFinalMessage(byte[] sendFinalMessage,
			String username) {

		try {
			this.username = username;
			Cipher cipher = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");

			convertor = new ConversionService();
			System.out.println("entering the proxy reply 2nd msg");

			// decrypt using the proxy private key

			System.out.println("1 entering the client final message");
			System.out.println("Username is: " + username);
			String pathToPrivateKey = "keys/" + username + ".pem";

			File f = new File(pathToPrivateKey);
			if (f.exists()) {
				System.out
						.println("There is a private key found for the proxy");
			}
			PEMReader in = new PEMReader(new FileReader(pathToPrivateKey),
					new PasswordFinderImpl());
			System.out.println("passed PEMReader");

			Object something = in.readObject();

			System.out.println("passed PEMReader reading");
			if (something.equals(null)) {
				System.out.println("In is nulled");
			} else {
				System.out.println("in is not null");
			}
			KeyPair keyPair = (KeyPair) something;
			PrivateKey privateKey = keyPair.getPrivate();
			in.close();

			byte[] textFromBase64 = Base64.decode(sendFinalMessage);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decryptedText = null;
			decryptedText = cipher.doFinal(textFromBase64);

			String finalConv = convertor.convert(decryptedText, String.class);
			String[] stringSplit = finalConv.split("[,]");

			List<String> sampleList = Arrays.asList(stringSplit);
			List<byte[]> byteList = new ArrayList<byte[]>();

			// the ok message // client challenge // proxy challenge // secret
			// key // iv-parameter
			for (int i = 0; i < sampleList.size(); i++) {
				byte[] b = Base64.decode(convertor.convert(sampleList.get(i),
						byte[].class));
				byteList.add(b);
			}

			// we need: 3 things
			// proxy-challenge: To send to the proxy
			// secret-key AND iv-perameter.

			// initialize the AES cipher with the proxy-challenge and iv
			// parameter

			byte[] proxyChallenge = byteList.get(2);
			byte[] secretKey = byteList.get(3);
			byte[] ivArr = byteList.get(4);
			System.out.println("Secrets in cli: " + secretKey + " " + ivArr);
			
			this.secretKey = secretKey;
			this.ivParArr = ivArr;

			SecretKeySpec encryptKey = new SecretKeySpec(secretKey,
					"AES/CTR/NoPadding");
			IvParameterSpec ivPar = new IvParameterSpec(ivArr);

			Cipher cipher1 = Cipher.getInstance("AES/CTR/NoPadding");

			cipher1.init(Cipher.ENCRYPT_MODE, encryptKey, ivPar);

			byte[] cipheredText = null;
			cipheredText = cipher1.doFinal(proxyChallenge);

			// base 64
			return Base64.encode(cipheredText);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public synchronized Object bytesToObject(byte[] message) {

		try {
			ByteArrayInputStream inStream = new ByteArrayInputStream(message);
			ObjectInputStream is = new ObjectInputStream(inStream);
			Object newObject = is.readObject();

			System.out.println("Test the object: " + newObject);
			try {
				inStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}

			System.out.println("Returning object conversion");
			return newObject;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;

	}

}
