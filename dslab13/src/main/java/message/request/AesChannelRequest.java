package message.request;

import message.Request;

/**
 * Retrieves the current amount of credits of the authenticated user.
 * <p/>
 * <b>Request</b>:<br/>
 * {@code !credits}<br/>
 * <b>Response:</b><br/>
 * {@code !credits &lt;total_credits&gt;}<br/>
 *
 * @see message.response.CreditsResponse
 */
public class AesChannelRequest implements Request {
	
	private static final long serialVersionUID = 1463905758859908545L;
	private byte[] message;
	public AesChannelRequest(byte[] message){
		this.message = message;
	}
	
	public byte[]getMessage(){
		return message;
	}
	
}
