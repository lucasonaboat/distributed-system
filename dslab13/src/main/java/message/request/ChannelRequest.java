package message.request;

import message.Request;

/**
 * Retrieves the current amount of credits of the authenticated user.
 * <p/>
 * <b>Request</b>:<br/>
 * {@code !credits}<br/>
 * <b>Response:</b><br/>
 * {@code !credits &lt;total_credits&gt;}<br/>
 *
 * @see message.response.CreditsResponse
 */
public class ChannelRequest implements Request {
	
	private static final long serialVersionUID = 3304869891526538041L;

	private byte[] message;
	public ChannelRequest(byte[] message){
		this.message = message;
	}
	
	public byte[]getMessage(){
		return message;
	}
	
}
