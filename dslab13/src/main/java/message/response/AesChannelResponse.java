package message.response;

import java.io.Serializable;

import message.Response;

/**
 * Buys additional credits for the authenticated user.
 * <p/>
 * <b>Request</b>:<br/>
 * {@code !buy &lt;credits&gt;}<br/>
 * <b>Response:</b><br/>
 * {@code !credits &lt;total_credits&gt;}<br/>
 *
 * @see message.request.BuyRequest
 */
public class AesChannelResponse implements Response {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4315722214263612694L;
	private final byte[] authenticationResponse;

	public AesChannelResponse(byte[] authenticationResponse) {
		this.authenticationResponse =authenticationResponse;
	}

	public byte[] getAuthenticationResponse() {
		return authenticationResponse;
	}

//	@Override
//	public String toString() {
//		return "!credits " + getCredits();
//	}
}
