package message.response;

import java.io.Serializable;

import message.Response;

/**
 * Buys additional credits for the authenticated user.
 * <p/>
 * <b>Request</b>:<br/>
 * {@code !buy &lt;credits&gt;}<br/>
 * <b>Response:</b><br/>
 * {@code !credits &lt;total_credits&gt;}<br/>
 *
 * @see message.request.BuyRequest
 */
public class ProxyAuthenticationResponse implements Response {
	
	private static final long serialVersionUID = 7060205212401984634L;
	private final byte[] authenticationResponse;

	public ProxyAuthenticationResponse(byte[] authenticationResponse) {
		this.authenticationResponse =authenticationResponse;
	}

	public byte[] getAuthenticationResponse() {
		return authenticationResponse;
	}

//	@Override
//	public String toString() {
//		return "!credits " + getCredits();
//	}
}
