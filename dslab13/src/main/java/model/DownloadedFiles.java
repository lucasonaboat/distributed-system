package model;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Contains information required to download files from a {@link server.IFileServer}.
 */
public final class DownloadedFiles implements Serializable {
	private static final long serialVersionUID = 289413562242240171L;

	private String filename;
	private ConcurrentMap<String, Integer> downloadedFiles;

	
	public DownloadedFiles() {
		super();
		downloadedFiles = new ConcurrentHashMap<String, Integer>();
	}

	public ConcurrentMap<String, Integer> returnDownloadedFiles() {
		return downloadedFiles;
	}
	
	public synchronized void incrementFile(String filename){
		if(downloadedFiles.containsKey(filename)){
			int newValue = downloadedFiles.get(filename).intValue() + 1;
			downloadedFiles.put(filename, newValue);
			
		}
	}
	
	public synchronized void addFile(String filename){
		if(!downloadedFiles.containsKey(filename)){
			downloadedFiles.put(filename, 1);
		}
	}
	
	
	
}
