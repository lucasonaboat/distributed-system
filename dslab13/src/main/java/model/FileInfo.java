package model;

public class FileInfo {
	
	private String name;
	private int version;
	
	public FileInfo(String name, int version) {
		this.name = name;
		this.version = version;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
