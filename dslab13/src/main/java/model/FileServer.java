package model;

import java.net.InetAddress;

public class FileServer {
	
	private InetAddress address;
	private int port;
	private long usage;
	private boolean online;
	private long lastAliveSignal;

	public FileServer(InetAddress address, int port, long usage, boolean online,long aliveSignal) {
		this.setAddress(address);
		this.setPort(port);
		this.setUsage(usage);
		this.setOnline(online);
		this.setLastAliveSignal(aliveSignal);
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long getUsage() {
		return usage;
	}

	public void setUsage(long usage) {
		this.usage = usage;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public long getLastAliveSignal() {
		return lastAliveSignal;
	}

	public void setLastAliveSignal(long lastAliveSignal) {
		this.lastAliveSignal = lastAliveSignal;
	}




}
