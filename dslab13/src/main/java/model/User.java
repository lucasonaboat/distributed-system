package model;

public class User {

	private String name;
	private long credits;
	private boolean online;
	private String password;
	private byte[] secretKey;
	private byte[] ivPar;
	
	public User(String name) {
		this.name = name;
	}
	
	public User(String name, long credits, boolean online, String password, byte[] secretKey, byte[]ivPar) {
		this.name = name;
		this.credits = credits;
		this.online = online;
		this.setPassword(password);
		this.setSessionKey(secretKey, ivPar);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public long getCredits() {
		return credits;
	}
	
	public void setCredits(long credits) {
		this.credits = credits;
	}

	public boolean isOnline() {
		return online;
	}
	
	public void setOnline(boolean online) {
		this.online = online;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public byte[] getSecretKey(){
		return secretKey;
	}
	public byte[] getIvPar(){
		return ivPar;
	}
	public void setSessionKey(byte[] secretKey, byte[] ivPar){
		this.secretKey = secretKey;
		this.ivPar = ivPar;
		
	}
	
	
}
