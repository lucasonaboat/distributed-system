package model;

public class UserSessions extends User {
	
	private String name;
	private long credits;
	private boolean online;
	private byte[] secretKey;
	private byte[] ivPar;
	private User user;
	
	public UserSessions(String user, byte[] secretKey, byte[]ivPar) {
		super(user);
		this.setSessionKey(secretKey, ivPar);
	}

	public byte[] getSecretKey(){
		return secretKey;
	}
	public byte[] getIvPar(){
		return ivPar;
	}
	public void setSessionKey(byte[] secretKey, byte[] ivPar){
		this.secretKey = secretKey;
		this.ivPar = ivPar;
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -54687509314712061L;

}
