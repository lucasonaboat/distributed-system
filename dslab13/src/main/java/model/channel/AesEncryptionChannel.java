package model.channel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import message.Request;

import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Base64Encoder;

public class AesEncryptionChannel extends ChannelDecorator{

	public AesEncryptionChannel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}
	
//	public String encryptA(byte[] secretKey, byte[] ivArr, Request source){
//		SecretKeySpec encryptKey = new SecretKeySpec(secretKey, "AES/CTR/NoPadding");
//		IvParameterSpec ivPar = new IvParameterSpec(ivArr);
//		Cipher cipher1 = null;
//		try {
//			cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
//			cipher1.init(Cipher.ENCRYPT_MODE, encryptKey, ivPar);
//			byte[]sourceBytes = objectToBytes(source);
//			SealedObject sealed = new SealedObject(source, cipher1);
//			
//			return Base64.encode(sealed);
//			
//		}catch (NoSuchAlgorithmException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (NoSuchPaddingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (InvalidKeyException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}catch (InvalidAlgorithmParameterException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}catch (IllegalBlockSizeException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		
//		return null;
//	}
//	
	
	
	
	public Cipher encryptCipher(byte[] secretKey, byte[] ivArr){
		SecretKeySpec encryptKey = new SecretKeySpec(secretKey, "AES/CTR/NoPadding");
		IvParameterSpec ivPar = new IvParameterSpec(ivArr);
		Cipher cipher1 = null;
		try {
			cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
			cipher1.init(Cipher.ENCRYPT_MODE, encryptKey, ivPar);
			return cipher1;
		}catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (InvalidAlgorithmParameterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return cipher1;
	}
	public Cipher decryptCipher(byte[] secretKey, byte[] ivArr){
		SecretKeySpec encryptKey = new SecretKeySpec(secretKey, "AES/CTR/NoPadding");
		IvParameterSpec ivPar = new IvParameterSpec(ivArr);
		Cipher cipher1 = null;
		try {
			cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
			cipher1.init(Cipher.DECRYPT_MODE, encryptKey, ivPar);
			return cipher1;
		}catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (InvalidAlgorithmParameterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return cipher1;
	}
	 
	
	public byte[]  encodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message){
		super.encodeAesEncryptionChannel(secretKey, ivArr, message);
		
		System.out.println("Length of secret key! " + secretKey.length);
		
		SecretKeySpec encryptKey = new SecretKeySpec(secretKey, "AES/CTR/NoPadding");
		IvParameterSpec ivPar = new IvParameterSpec(ivArr);
		
		Cipher cipher1;
		try {
			cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
			cipher1.init(Cipher.ENCRYPT_MODE, encryptKey, ivPar);
			//SealedObject sealedObject = new SealedObject(message, cipher1);
			
			// base 64
			//byte[] b = sealedObject.toString().getBytes();
//			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
//			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			
			byte[]finalBytes = cipher1.doFinal(message);
//			
//			objectOut.writeObject(sealedObject);
//			byte[] sealedBytes = byteArrayOs.toByteArray();
//			objectOut.close();
//			byteArrayOs.close();
			
			return Base64.encode(finalBytes);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "Encryption did not work".getBytes();
		
		
		
		
		
	}
	
	public synchronized byte[] objectToBytes(Object message) {
		try {
			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			objectOut.writeObject(message);
			byte[] sealedBytes = byteArrayOs.toByteArray();
			objectOut.close();
			byteArrayOs.close();

			return sealedBytes;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public synchronized Object bytesToObject(byte[] message){
		
		try {
			ByteArrayInputStream inStream = new ByteArrayInputStream(message);
		    ObjectInputStream is = new ObjectInputStream(inStream);
			Object newObject = is.readObject();
			
			System.out.println("Test the object: " + newObject);
			try{
				inStream.close();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Ignore exception bytes to object Aes encryption channel");
			}try{
				if(is != null){
					is.close();
				}
			}catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Ignore exception bytes to object Aes encryption channel");
			}
			
			System.out.println("Returning object conversion");
			return newObject;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;
		
	    
		
	}
	
	
	public byte[]  decodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message){
		super.decodeAesEncryptionChannel(secretKey, ivArr, message);
		
		byte[] secretKey1 = secretKey;
		byte[] ivArr1 = ivArr;
		
		
		SecretKeySpec encryptKey = new SecretKeySpec(secretKey1, "AES/CTR/NoPadding");
		IvParameterSpec ivPar = new IvParameterSpec(ivArr1);
		
		Cipher cipher1;
		try {
			if(message ==null){
				System.out.println("The message is null");
			}
			System.out.println("Message to decode is: " + message);
			String asString = new String(message);
			System.out.println("As string " + asString);
			
			byte[]messageBytes = Base64.decode(message);
			
			System.out.println("Message b64 decoded: " + messageBytes);
			cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
			cipher1.init(Cipher.DECRYPT_MODE, encryptKey, ivPar);
			
			byte[] cipheredText = null;
			cipheredText = cipher1.doFinal(messageBytes);
			
			//cipheredText = cipher1.doFinal(message);

			// base 64
			
			System.out.println("Decoded array aes: " + cipheredText);
			return cipheredText;
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

}
