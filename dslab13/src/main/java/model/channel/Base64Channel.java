package model.channel;

import org.bouncycastle.util.encoders.Base64;

public class Base64Channel extends ChannelDecorator{

	public Base64Channel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}
	
	
	public byte[]  decodeBase64(byte[] message){
		super.decodeBase64(message);
		byte[] encryptedMessage = Base64.decode(message);
		return encryptedMessage;
		
	}
	
	
	public byte[]  encodeBase64(byte[] encryptedMessage){
		super.encodeBase64(encryptedMessage);
		byte[] base64Message = Base64.encode(encryptedMessage);
		return base64Message;
	}

}
