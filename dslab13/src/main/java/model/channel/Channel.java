package model.channel;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;

import sun.security.tools.KeyTool;

import convert.ConversionService;

public class Channel extends ChannelDecorator {

	public Channel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}

	// implement the ways to send and receive Objects over a Socket
	// MODE is the encryption/decryption mode
	// KEY is either a private, public or secret key
	// IV is an init vector, needed for AES

	public byte[] clientLoginRequest(String username) {
		// !login <username> <client-challenge>
		String message = "!login";
		String delimeter = ",";
		byte[] delimeterArray = delimeter.getBytes();
		
		
		
		try {
			// initialize the cipher
			Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");

			ConversionService convertor = new ConversionService();

			//byte[] keyBytez = new byte[1024];
			String pathToPublicKey = "keys/proxy.pub.pem";
			File f = new File(pathToPublicKey);
			if (f.exists()) {
				System.out
						.println("There is a public key found for the proxy");
			}
			else{
				String noKeyFound = "There was no key found";
				return noKeyFound.getBytes();
			}
			PEMReader in = new PEMReader(new FileReader(pathToPublicKey));
			PublicKey publicKey = (PublicKey) in.readObject();
			// System.out.println(publicKey);
			
			// initialize the cipher
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] cipherText = null;
			in.close();
			// client challenge generate random number, change to byte array and
			// then encode in base 64
			SecureRandom secureRandom = new SecureRandom();
			final byte[] number = new byte[32];
			secureRandom.nextBytes(number);

			byte[] clientChallenge = Base64.encode(number);
			
			
			byte[]usernameByteArr = convertor.convert(username, byte[].class);
			byte[] messageEncrypted = convertor.convert(message, byte[].class);
			
			System.out.println("lengths: message: " + messageEncrypted.length
					+ "challenge: " + clientChallenge.length);
			
			int breaksBetween = 2;
			
			// length = message + break + username + break + client challenge
			byte[] concatenatedByteArr = new byte[messageEncrypted.length + usernameByteArr.length + 
					+ clientChallenge.length + (delimeterArray.length *breaksBetween)];
			System.out.println("Total length: " + concatenatedByteArr.length);
			
			// !login 
			System.arraycopy(messageEncrypted, 0, concatenatedByteArr, 0,
					messageEncrypted.length);
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, messageEncrypted.length, delimeterArray.length);
			
			// username
			System.arraycopy(usernameByteArr, 0, concatenatedByteArr, messageEncrypted.length + delimeterArray.length
					, usernameByteArr.length);
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, 
					messageEncrypted.length + usernameByteArr.length + delimeterArray.length , delimeterArray.length);
			
			// <client challenge>
			System.arraycopy(clientChallenge, 0, concatenatedByteArr,
					messageEncrypted.length + usernameByteArr.length + (delimeterArray.length *2), clientChallenge.length);

			cipherText = cipher.doFinal(concatenatedByteArr);
			System.out.println("Ciphertext " + cipherText);
			
			byte[] encryptedBase64 = Base64.encode(cipherText);
			System.out.println("Ciphertext base 64 " + encryptedBase64);
			return encryptedBase64;
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
