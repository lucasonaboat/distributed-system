package model.channel;

import java.io.ObjectInputStream;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;

import model.User;

public class ChannelDecorator extends TcpChannel {
	
	protected TcpChannel channelDecorator;
	
	public ChannelDecorator(TcpChannel channelDecorator){
		this.channelDecorator = channelDecorator;
	}
	
	public byte[] decodeBase64(byte[] message) {
		
		return channelDecorator.decodeBase64(message);
	}

	public byte[] encodeBase64(byte[] encryptedMessage) {
		
		return channelDecorator.encodeBase64(encryptedMessage);
	}
	
	// implement ways to send and receive Objects over a Socket
	
	public byte[] clientLoginRequest(String username){
		return channelDecorator.clientLoginRequest(username);
	}

	public byte[] proxyReturnMessage(byte[] clientChallenge, ConcurrentHashMap<String,User> userList) {
		return channelDecorator.proxyReturnMessage(clientChallenge, userList);
	}
	public byte[] clientSendFinalMessage(byte[] sendFinalMessage, String username) {
		return channelDecorator.clientSendFinalMessage(sendFinalMessage, username);
	}
	
	public byte[] encodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message){
		return channelDecorator.encodeAesEncryptionChannel(secretKey, ivArr, message);
	}
	
	public byte[]  decodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message){
		return channelDecorator.decodeAesEncryptionChannel(secretKey, ivArr, message);
	}
	public String returnUser(){
		return channelDecorator.returnUser();
	}
	public Cipher encryptCipher(byte[] secretKey, byte[] ivArr){
		return channelDecorator.encryptCipher(secretKey, ivArr);
	}
	
	public Cipher decryptCipher(byte[] secretKey, byte[] ivArr){
		return channelDecorator.decryptCipher(secretKey, ivArr);
	}

	public String generateHmac(String secretKey, String message) {
		return channelDecorator.generateHmac(secretKey, message);
	}

	public boolean verifyHmac(String secretKey, String hashMac, String recievedContents) {
		return channelDecorator.verifyHmac(secretKey, hashMac, recievedContents);
	}

	public byte[] returnProxyChallenge() {
		return channelDecorator.returnProxyChallenge();
	}
}
