package model.channel;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;



import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Base64;

import convert.ConversionService;

public class ClientFinalMessageChannel extends ChannelDecorator{

	public ClientFinalMessageChannel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}
	private ConversionService convertor;
	private String username;
	private  class PasswordFinderImpl implements PasswordFinder{
		
		@Override
		public char[] getPassword() {
			// TODO Auto-generated method stub
			System.out.println("getting password");
			if(username.equals("alice")){
				return "12345".toCharArray();
			}
			else if (username.equals("bill")) {
				return "23456".toCharArray();
			}{
				return "11111".toCharArray();
			}
			
		}
		
	}

	public byte[] clientSendFinalMessage(byte[] sendFinalMessage, String username){
		super.clientSendFinalMessage(sendFinalMessage, username);
		this.username = username;
		
		try {
			Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			
			convertor = new ConversionService();
			System.out.println("entering the proxy reply 2nd msg");
			
			// decrypt using the proxy private key
			
			System.out.println("1 entering the client final message");
			String pathToPrivateKey = "keys/" + username + ".pem";

			File f = new File(pathToPrivateKey);
			if (f.exists()) {
				System.out
						.println("There is a private key found for the proxy");
			}
			else{
				String noKeyFound = "There was no key found";
				return noKeyFound.getBytes();
			}
			PEMReader in = new PEMReader(new FileReader(pathToPrivateKey),new PasswordFinderImpl()); 
			System.out.println("passed PEMReader");
			
			Object something = in.readObject();
			
			System.out.println("passed PEMReader reading");
			if(something.equals(null)){
				System.out.println("In is nulled");
			}
			else{
				System.out.println("in is not null");
			}
			KeyPair keyPair = (KeyPair)something;
			PrivateKey privateKey = keyPair.getPrivate();
			in.close();
			
			byte[] textFromBase64 = Base64.decode(sendFinalMessage);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decryptedText = null;
			decryptedText = cipher.doFinal(textFromBase64);
			
			String finalConv = convertor.convert(decryptedText, String.class);
			String[] stringSplit = finalConv.split("[,]");

			List<String> sampleList = Arrays.asList(stringSplit);
			List<byte[]> byteList = new ArrayList<byte[]>();
			
			// the ok message // client challenge // proxy challenge // secret key // iv-parameter
			for (int i = 0; i < sampleList.size(); i++) {
				byte[]b = Base64.decode(convertor.convert(sampleList.get(i), byte[].class));
				byteList.add(b);
			}
			
			
			// we need: 3 things
			// proxy-challenge: To send to the proxy
			// secret-key AND iv-perameter. 
			
			// initialize the AES cipher with the proxy-challenge and iv parameter
			
			byte[] proxyChallenge = byteList.get(2);
			byte[] secretKey = byteList.get(3);
			byte[] ivArr = byteList.get(4);
			
			SecretKeySpec encryptKey = new SecretKeySpec(secretKey, "AES/CTR/NoPadding");
			IvParameterSpec ivPar = new IvParameterSpec(ivArr);
			
			Cipher cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
			
			cipher1.init(Cipher.ENCRYPT_MODE, encryptKey, ivPar);
			
			byte[] cipheredText = null;
			cipheredText = cipher1.doFinal(proxyChallenge);

			// base 64
			return Base64.encode(cipheredText);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	

}
