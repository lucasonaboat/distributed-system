package model.channel;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

public class HmacChannel extends ChannelDecorator {

	public HmacChannel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}

	public String generateHmac(String secretKey, String message) {

		SecretKeySpec encryptKey = new SecretKeySpec(secretKey.getBytes(),
				"HmacSHA256");
		
		System.out.println("HMAC Line 1");
		try {
			Mac hMacGenerate = Mac.getInstance("HmacSHA256");
			hMacGenerate.init(encryptKey);
			System.out.println("HMAC generate1: " + hMacGenerate);
			hMacGenerate.update(message.getBytes());
			System.out.println("HMAC generate2: " + hMacGenerate);
			
			byte[] hash = Base64.encode(hMacGenerate.doFinal());
			System.out.println("HMAC hash: " + hash);
			
			String finalString = new String(hash);
			System.out.println("HMAC string: " + finalString);
			return finalString;

		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean verifyHmac(String secretKey, String hashMac, String recievedContents) {
		SecretKeySpec encryptKey = new SecretKeySpec(secretKey.getBytes(),
				"HmacSHA256");
		try {
			System.out.println("secret key: " + secretKey);
			Mac hMacGenerate = Mac.getInstance("HmacSHA256");
			hMacGenerate.init(encryptKey);
			hMacGenerate.update(recievedContents.getBytes());
			byte[]hash = Base64.encode(hMacGenerate.doFinal());
			
			String recievedHmacString = hashMac;
			String computedHmacString = new String(hash);
			
			System.out.println("Compare: " + recievedHmacString + " " + computedHmacString);
			return MessageDigest.isEqual(hash, hashMac.getBytes());
			
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return false;
	}

}
