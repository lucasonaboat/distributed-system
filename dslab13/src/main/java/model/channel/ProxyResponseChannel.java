package model.channel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import model.User;
import model.UserSessions;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Base64;

import convert.ConversionService;

public class ProxyResponseChannel extends ChannelDecorator {

	public ProxyResponseChannel(TcpChannel decoratedChannel) {
		super(decoratedChannel);
	}

	private ConversionService convertor;
	private  class PasswordFinderImpl implements PasswordFinder{
		
		@Override
		public char[] getPassword() {
			// TODO Auto-generated method stub
			System.out.println("getting password");
			return "12345".toCharArray();
		}
		
	}
	public synchronized byte[] objectToBytes(Object message) {
		try {
			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			objectOut.writeObject(message);
			byte[] sealedBytes = byteArrayOs.toByteArray();
			objectOut.close();
			byteArrayOs.close();

			return sealedBytes;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	private String username;
	public String returnUser(){
		return username;
	}
	
	private byte[] proxyChallenge;
	public byte[] returnProxyChallenge(){
		return proxyChallenge;
	}
	
	public byte[] proxyReturnMessage(byte[] clientChallenge, ConcurrentHashMap<String,User> userList) {
		// !ok <client-challenge> <proxy-challenge> <secret-key> <iv-parameter>.

		super.proxyReturnMessage(clientChallenge, userList);
		String noKeyFound = "There was no key found";
		try {
			convertor = new ConversionService();
			System.out.println("entering the proxy reply 2nd msg");
			
			// decrypt using the proxy private key
			Cipher cipher = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");

			System.out.println("1 entering the proxy reply 2nd msg");
			String pathToPrivateKey = "keys/proxy.pem";

			File f = new File(pathToPrivateKey);
			if (f.exists()) {
				System.out
						.println("There is a private key found for the proxy");
			}
			else{
				
				return noKeyFound.getBytes();
			}
			PEMReader in = new PEMReader(new FileReader(pathToPrivateKey),new PasswordFinderImpl()); 
			System.out.println("passed PEMReader");
			
			Object something = in.readObject();
			
			System.out.println("passed PEMReader reading");
			if(something.equals(null)){
				System.out.println("In is nulled");
			}
			else{
				System.out.println("in is not null");
			}
			KeyPair keyPair = (KeyPair)something;
			PrivateKey privateKey = keyPair.getPrivate();
			in.close();
			

			// PEMReader in = new PEMReader(new FileReader(pathToPublicKey));
			// PrivateKey proxyPrivateKey = (PrivateKey)in.readObject();
			// in.close();
			
			byte[] textFromBase64 = Base64.decode(clientChallenge);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decryptedText = null;
			decryptedText = cipher.doFinal(textFromBase64);

			// message now decrypted
			// now split the original !login <username> <client-challenge> into
			// two sections to get the challenge
			// assume the challenge part is 44 bytes
			
			ConversionService convertor = new ConversionService();
			String finalConv = convertor.convert(decryptedText, String.class);
			String[] stringSplit = finalConv.split("[,]");

			List<String> sampleList = Arrays.asList(stringSplit);
			for (int i = 0; i < sampleList.size(); i++) {
				System.out.println("List: " + i + " " + sampleList.get(i));
			}
			
			// message + username + client challenge
			String username = sampleList.get(1);
			System.out.println("Username is: " + username);
			this.username = username;
			
			// the ok message
			String ok = "!Ok";
			byte[] okArr = convertor.convert(ok, byte[].class);
			int length1 = okArr.length;
			System.out.println("okMessage: " + okArr + "Length1: " + length1);
			byte[] ok1 = Base64.encode(okArr);

			// client challenge
			byte[] cliChal = convertor.convert(sampleList.get(2), byte[].class);
			byte[] decodedClientChallenge = Base64.decode(cliChal);
			int length2 = decodedClientChallenge.length;
			System.out.println("client challenge: " + decodedClientChallenge + "Length2: " + length2);
			byte[] clientChallenge2 = Base64.encode(decodedClientChallenge);
			
			// proxy challenge
			SecureRandom secureRandom = new SecureRandom();
			final byte[] number = new byte[32];
			secureRandom.nextBytes(number);
			int length3 = number.length;
			System.out.println("proxy Challenge: " + number + "Length3: " + length3);
			System.out.println("Proxy challenge to String: " + number.toString());
			byte[] proxyChallenge3 = Base64.encode(number);
			this.proxyChallenge = proxyChallenge3;
			

			// secret key

			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(256);
			SecretKey secretKey = keyGenerator.generateKey();
			
			//String secretCodeToString = secretKey.toString();
			byte[] secretCodeToByteArr = secretKey.getEncoded();
			int length4 = secretCodeToByteArr.length;
			System.out.println("Secret key: " + secretCodeToByteArr + "Length4: " + length4);
			byte[] secretKey4 = Base64.encode(secretCodeToByteArr);

			// iv-parameter
			
			SecureRandom ivRandom = new SecureRandom();
			final byte[] ivNumber = new byte[16];
			ivRandom.nextBytes(ivNumber);
			IvParameterSpec par = new IvParameterSpec(ivNumber);
			int length5 = par.getIV().length;
			System.out.println("iv parameter: " + par.getIV() + "Length: " + length5);
			
			
			// set the sessionKey in the model
			
			//UserSessions userSession = new UserSessions(username, secretCodeToByteArr, par.getIV());
			if(!userList.containsKey(username)){
				return noKeyFound.getBytes();
			}
			User oldUser = userList.get(username);
			
			
			User use1 = new User(oldUser.getName(), oldUser.getCredits(), oldUser.isOnline(), oldUser.getPassword(),  
					secretCodeToByteArr, par.getIV());
			userList.put(username, use1);
			
			System.out.println(" Proxy response: " + use1 );
			System.out.println(" Proxy response: " + use1.getIvPar() );
			System.out.println(" Proxy response: " + use1.getSecretKey() );
			byte[] ivParameter5 = Base64.encode(par.getIV());

			// the ok message // client challenge // proxy challenge // secret key // iv-parameter
			// need 4 breaks
			
			String delimeter = ",";
			byte[] delimeterArray = delimeter.getBytes();
			
			byte[] concatenatedByteArr = new byte[ok1.length
					+ clientChallenge2.length + proxyChallenge3.length
					+ secretKey4.length + ivParameter5.length + (delimeterArray.length * 4)];
			
			// concatenate the 5 arrays into one and then encode into base64
			int lengthCounter = 0;
			
			// ok message
			System.arraycopy(ok1, 0, concatenatedByteArr, lengthCounter, ok1.length);
			lengthCounter+= ok1.length;
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, lengthCounter, delimeterArray.length);
			lengthCounter+= delimeterArray.length;
			
			// client challenge
			System.arraycopy(clientChallenge2, 0, concatenatedByteArr,lengthCounter, clientChallenge2.length);
			lengthCounter+= clientChallenge2.length;
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, lengthCounter, delimeterArray.length);
			lengthCounter+= delimeterArray.length;
			
			// proxy challenge
			System.arraycopy(proxyChallenge3, 0, concatenatedByteArr, lengthCounter, proxyChallenge3.length);
			lengthCounter += proxyChallenge3.length;
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, lengthCounter, delimeterArray.length);
			lengthCounter+= delimeterArray.length;
			
			// secret key
			System.arraycopy(secretKey4, 0, concatenatedByteArr, lengthCounter, secretKey4.length);
			lengthCounter += secretKey4.length;
			
			// break
			System.arraycopy(delimeterArray, 0, concatenatedByteArr, lengthCounter, delimeterArray.length);
			lengthCounter+= delimeterArray.length;
			
			//iv-parameter
			System.arraycopy(ivParameter5, 0, concatenatedByteArr, lengthCounter, ivParameter5.length);
			
			// encode using the users public key
			System.out.println("Searching user public key for user: "
					+ username);
			
			
			String pathPub = "keys/"+username+".pub.pem";
			String pathToPublicKey = pathPub.replaceAll("\\s","");
			File g = new File(pathToPublicKey);
			if (g.exists()) {
				System.out
						.println("There is a public key found for the proxy");
			}
			else{
				
				return noKeyFound.getBytes();
			}
			
			
			
			System.out.println("path: " + pathToPublicKey + "Length: " + pathToPublicKey.length());
			File publicFile = new File(pathToPublicKey);
			
			PEMReader pemIn = new PEMReader(new FileReader(publicFile));
			PublicKey publicKey = (PublicKey) pemIn.readObject();
			// System.out.println(publicKey);
			pemIn.close();
			// initialize the cipher
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] cipheredText = null;
			
			
			cipheredText = cipher.doFinal(concatenatedByteArr);

			// base 64
			return Base64.encode(cipheredText);
						
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

}
