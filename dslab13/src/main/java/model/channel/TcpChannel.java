package model.channel;

import java.io.ObjectInputStream;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;

import model.User;

public abstract class TcpChannel {
	
	public abstract byte[]  decodeBase64(byte[] message);
	public abstract byte[]  encodeBase64(byte[] encryptedMessage);
	
	// add the model for ways to send and receive Objects over a Socket

	public abstract byte[] clientLoginRequest(String username);
	
	public abstract byte[] proxyReturnMessage(byte[] clientChallenge, ConcurrentHashMap<String,User> userList);
	
	public abstract byte[] clientSendFinalMessage(byte[] sendFinalMessage, String username);
	
	public abstract byte[] encodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message);
	
	public abstract byte[]  decodeAesEncryptionChannel(byte[] secretKey, byte[] ivArr, byte[] message);
	
	public abstract String returnUser();
	
	public abstract Cipher encryptCipher(byte[] secretKey, byte[] ivArr);
	
	public abstract Cipher decryptCipher(byte[] secretKey, byte[] ivArr);
	
	public abstract String generateHmac(String secretKey, String message);
	
	public abstract boolean verifyHmac(String secretKey, String hashMac, String recievedContents);
	
	public abstract byte[] returnProxyChallenge();
}
