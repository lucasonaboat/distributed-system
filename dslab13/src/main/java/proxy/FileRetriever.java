package proxy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bouncycastle.ocsp.RespData;

import message.Request;
import message.Response;
import message.request.ListRequest;
import message.request.VersionRequest;
import message.response.ListResponse;
import message.response.VersionResponse;
import model.FileInfo;
import model.channel.HmacChannel;
import model.channel.SimpleTcpChannel;
import model.channel.TcpChannel;

public class FileRetriever implements Runnable {


	private ConcurrentLinkedQueue<FileInfo> files;
	private InetAddress addr;
	private int port;
	private ObjectOutputStream out = null;
	private ObjectInputStream in = null;
	private Socket socket = null;
	private String hmacDirectory;
	public FileRetriever(ConcurrentLinkedQueue<FileInfo> files, InetAddress addr, int port, String hmacDirectory) {
		this.files = files;
		this.addr = addr;
		this.port = port;
		this.hmacDirectory = hmacDirectory;
	}
		
	
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		
		File file = new File(hmacDirectory);
		String calculatedHmac = "";
		if(!file.exists()){
			System.out.println("No existing hmac file");
		}
		StringBuilder hmacFile = new StringBuilder();
		Scanner scan;
		try {
			scan = new Scanner(file);
			while(scan.hasNextLine()){
				hmacFile.append(scan.nextLine());
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String keyContents = hmacFile.toString();
		
		ListResponse list = null;
		//System.out.println(addr);
		try {
			socket = new Socket(addr.getHostName(), port);
			System.out.println("Connecting to: " + addr.getHostAddress() + " port: " + port);
			//System.out.println(socket.getInetAddress());
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			
		} catch(IOException e) {
			System.err.println("FileRetriever failed to connect");
		}
		try {
			Request req = new ListRequest();
			
			
			TcpChannel chan = new SimpleTcpChannel();
			chan = new HmacChannel(chan);
			//byte[]messageB = objectToBytes(req);
			
			String serializedString = objectToString(req);
			
			
			System.out.println("Proxy Fileretriver key and msg: " + keyContents);
			calculatedHmac = chan.generateHmac(keyContents, serializedString);		
			System.out.println("Proxy Fileretriver msg: " + serializedString);
			
			System.out.println("Proxy Fileretriver hmac is: " + calculatedHmac);
			
			System.out.println("Request to serialized string: " + serializedString);
			
			List<Object>toServer = new ArrayList<Object>();
			toServer.add(calculatedHmac);
			toServer.add(req);
			out.writeObject(toServer);
			out.flush();
		} catch (Exception e) {
			System.err.println("FileRetriever failed to write");
		}
		try {
			
			Object input = in.readObject();
			if(input instanceof List<?>){
				TcpChannel channel = new SimpleTcpChannel();
				channel = new HmacChannel(channel);
				
				System.out.println("Is arraylist");
				List<Object>inputList = new ArrayList<Object>();
				inputList.addAll((Collection<? extends String>) input);
				if(inputList.size()==2){
					String recievedContents = objectToString(inputList.get(1));
					boolean verify = channel.verifyHmac(keyContents, inputList.get(0).toString(), recievedContents); // 0 is hashmac 1 is contents
					if(verify){
						list = (ListResponse)inputList.get(1);
					}
				}
			}
			
		} catch (Exception e) {
			System.err.println("FileRetriever failed to read");
		}
		//System.out.println(list.getFileNames().isEmpty());
		try {
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
			in.close();
			out.close();
		} catch (Exception e) {
		}
		
		boolean fileExists = false;
		VersionResponse version = null;
		if(list != null){
		for(String s : list.getFileNames()) {
			//checkversion
			
			
			try {
				socket = new Socket(addr.getHostName(), port);
				out = new ObjectOutputStream(socket.getOutputStream());
				in = new ObjectInputStream(socket.getInputStream());
				
				
			} catch (IOException e) {

			}
			
			VersionResponse versionResp = null;
			try {
				
				VersionRequest versReq= new VersionRequest(s);
				TcpChannel chan = new SimpleTcpChannel();
				chan = new HmacChannel(chan);
				String generatedHmac = chan.generateHmac(keyContents, objectToString(versReq));
				List<Object>toServer = new ArrayList<Object>();
				toServer.add(generatedHmac);
				toServer.add(versReq);
				out.writeObject(toServer);
				out.flush();
				
				Object input = in.readObject();
				List<Object>inputList = new ArrayList<Object>();
				inputList.addAll((Collection<? extends String>) input);
				if(inputList.size()==2){
					String recievedContents = objectToString(inputList.get(1));
					boolean verify = chan.verifyHmac(keyContents, inputList.get(0).toString(), recievedContents); // 0 is hashmac 1 is contents
					if(verify){
						versionResp = (VersionResponse)inputList.get(1);
					}
				}
			} catch (Exception e) {

			}
			
			try {
				socket.shutdownInput();
				socket.shutdownOutput();
				socket.close();
				in.close();
				out.close();
			} catch (IOException e) {
			
			}
			
			
			for(FileInfo f : files) {
				synchronized(files) {
					if(s.equals(f.getName())) {
						fileExists = true;
						if(versionResp.getVersion() > f.getVersion()) {
							f.setVersion(versionResp.getVersion());
						}
						break;
					}
				}
			}	
			
			if(!fileExists) {
				files.add(new FileInfo(s,versionResp.getVersion()));
			}
		}
			
	}
		
	}
	
	public void shutdown() {
		try {
			socket.close();
			in.close();
			out.close();
		} catch (IOException e) {
			
		}
		
	}
	public synchronized byte[] objectToBytes(Object message) {
		try {
			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			objectOut.writeObject(message);
			byte[] sealedBytes = byteArrayOs.toByteArray();
			objectOut.close();
			byteArrayOs.close();

			return sealedBytes;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	public synchronized Object bytesToObject(byte[] message) {

		try {
			ByteArrayInputStream inStream = new ByteArrayInputStream(message);
			ObjectInputStream is = new ObjectInputStream(inStream);
			Object newObject = is.readObject();

			System.out.println("Test the object: " + newObject);
			try {
				inStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}
			try {
				//if (is != null) {
					is.close();
				//}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}

			System.out.println("Returning object conversion");
			return newObject;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;

	}
	
	public synchronized String objectToString(Object object){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream objectOs = null;
		try {
			objectOs = new ObjectOutputStream(baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.writeObject(object);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toString();

	}
	public synchronized Object stringToResponse(String message) {

		try {
			System.out.println("Original string: " + message);
			byte[]msgBytes = message.getBytes();
			
			ByteArrayInputStream inStream = new ByteArrayInputStream(msgBytes);
			ObjectInputStream is = new ObjectInputStream(inStream);
			if(is !=null){
				System.out.println("The instream is not null");
			}
			Object ob = is.readObject();
			
			if(ob instanceof Response){
				System.out.println("is instance of response");
				Response newObject = (Response)ob;
				return newObject;
			}
			
			System.out.println("Returning object conversion");
			return ob;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;

	}

}
