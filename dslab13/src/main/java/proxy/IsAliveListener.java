package proxy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import model.FileInfo;
import model.FileServer;

public class IsAliveListener implements Runnable {
	
	private ConcurrentLinkedQueue<FileServer> fileServers;
	private ConcurrentLinkedQueue<FileInfo> files;
	private DatagramSocket udpSocket;
	private boolean listen = true;
	private int timeout;
	private int checkPeriod;
	private Timer serverSignal;
	private ArrayList<FileRetriever> frs = new ArrayList<FileRetriever>();
	private String hmacDirectory;
	
	public IsAliveListener(DatagramSocket socket, ConcurrentLinkedQueue<FileServer> fileServers, int timeout, int checkPeriod, ConcurrentLinkedQueue<FileInfo> files, String hmacDirectory) {
		this.udpSocket = socket;
		this.fileServers = fileServers;
		this.timeout = timeout;
		this.checkPeriod = checkPeriod;
		this.files = files;
		this.hmacDirectory = hmacDirectory;
	}
	@Override
	public void run() {
		
		TimerTask action = new TimerTask() {
            @Override
			public void run() { checkServers(); }
        };
        
        serverSignal = new Timer();
        serverSignal.schedule(action, 3000, checkPeriod);
		
		byte[] buf = new byte[256];
		DatagramPacket receivePacket;
		String received="";
		try {
			while(listen) {
				receivePacket = new DatagramPacket(buf, buf.length);
				udpSocket.receive(receivePacket);
				received = new String(receivePacket.getData(), 0, receivePacket.getLength());
				if(received.length() != 0){
                    String values[]=null;
                    if(received.length() > 5 && received.substring(0,6).equals("!alive")){
                        values=received.split(" ",2);
                        InetAddress addr = receivePacket.getAddress();
                        int port = Integer.parseInt(values[1]);
                        boolean fs_registered = false;
                        if (!fileServers.isEmpty()) {
	                        for (FileServer f : fileServers) {
	                        	if((f.getPort() == port) && f.getAddress().equals(addr) ) {
	                        		fs_registered = true;
	                        		//System.out.println("ONLINE");
	                        		synchronized(fileServers) {
		                        		f.setOnline(true);
		                        		f.setLastAliveSignal(new Date().getTime());
	                        		}
	                        		break;
	                        	}
	                        }
                        }
                        if(!fs_registered) {
                        	FileServer fs = new FileServer(addr,port,0,true, new Date().getTime());
                        	fileServers.add(fs);
                        	FileRetriever retriever = new FileRetriever(files, addr, port, hmacDirectory);
                        	frs.add(retriever);
                        	Thread frThread = new Thread(retriever);
                        	frThread.start();
                        }                      
                    }
                    received="";
                }
			}
		} catch (IOException e) {
			System.err.println("Failed to receive packet");
		}
	}
	
	public void checkServers() {
		for(FileServer f : fileServers) {
			if((new Date().getTime() - f.getLastAliveSignal()) > timeout) {
				f.setOnline(false);
			}
		}			        
	}
	
	public void shutdown() {
		listen = false;	
		if(!frs.isEmpty()) {
			for(FileRetriever f : frs) {
				if(f != null) f.shutdown();			
			}
		}
		serverSignal.cancel();
		serverSignal.purge();
		udpSocket.close();
	}

}
