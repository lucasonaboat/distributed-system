package proxy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SealedObject;
import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.util.encoders.Base64;

import message.Request;
import message.Response;
import message.request.AesChannelRequest;
import message.request.BuyRequest;
import message.request.ChannelRequest;
import message.request.CreditsRequest;
import message.request.DownloadTicketRequest;
import message.request.InfoRequest;
import message.request.ListRequest;
import message.request.LoginRequest;
import message.request.LogoutRequest;
import message.request.UploadRequest;
import message.request.VersionRequest;
import message.response.AesChannelResponse;
import message.response.BuyResponse;
import message.response.CreditsResponse;
import message.response.DownloadTicketResponse;
import message.response.InfoResponse;
import message.response.ListResponse;
import message.response.LoginResponse;
import message.response.LoginResponse.Type;
import message.response.MessageResponse;
import message.response.ProxyAuthenticationResponse;
import message.response.VersionResponse;
import model.DownloadTicket;
import model.DownloadedFiles;
import model.FileInfo;
import model.FileServer;
import model.User;
import model.channel.AesEncryptionChannel;
import model.channel.HmacChannel;
import model.channel.ProxyResponseChannel;
import model.channel.SimpleTcpChannel;
import model.channel.TcpChannel;
import util.ChecksumUtils;
import cli.Command;
import convert.ConversionService;

public class Proxy implements IProxy, Runnable {

	private ConcurrentHashMap<String, User> userList;
	private ConcurrentLinkedQueue<FileServer> fileServers;
	private ConcurrentLinkedQueue<FileInfo> files;
	private Socket socket;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private String currentUser = null;
	private DownloadedFiles downloadedFiles;
	private TcpChannel encryptChannel;
	private TcpChannel decryptChannel;
	private byte[] secretKey;
	private byte[] ivParArr;
	private ConversionService convert;
	private String noKeyFound = "There was no key found";
	private String hmacDirectory;

	public Proxy(ConcurrentHashMap<String, User> userList, Socket socket,
			ConcurrentLinkedQueue<FileServer> fileServers,
			ConcurrentLinkedQueue<FileInfo> files,
			DownloadedFiles downloadedFiles, String hmacDirectory) {
		this.userList = userList;
		this.socket = socket;
		this.fileServers = fileServers;
		this.files = files;
		this.downloadedFiles = downloadedFiles;
		this.hmacDirectory = hmacDirectory;
	}



	@Override
	public void run() {

		try {
			encryptChannel = new SimpleTcpChannel();
			encryptChannel = new AesEncryptionChannel(encryptChannel);

			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());

		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Failed to create in-/ouputstream");
		}
		Object input = null;

		while (!socket.isClosed()) {
			try {
				input = in.readObject();

			} catch (ClassNotFoundException e) {

			} catch (IOException e) {
			} catch (NullPointerException e) {
			}
			try {

				Object resp = null;
				Response requestNoSecretKey = null;
				Response returnResponse = null;
				boolean isLogoutRequest = false;
				if (input != null) {
					if (input instanceof ChannelRequest) {
						System.out
								.println("Is instance of channel request. (1st message)");
						resp = authenticateRSA((ChannelRequest) input);
					} else if (input instanceof AesChannelRequest) {
						System.out
								.println("Is instance of aes channel request. (3rd message)");
						boolean verify = authenticateFinalMessage((AesChannelRequest) input);
						if(verify==false){
							System.out.println("Validation failed. Logging out");
							isLogoutRequest = true;
							returnResponse = new LoginResponse(Type.WRONG_CREDENTIALS);
						}
						// need to assert this is a good form with regex.
					} else if (input instanceof SealedObject) {
						SealedObject sealed = (SealedObject) input;
						TcpChannel cha = new SimpleTcpChannel();
						cha = new AesEncryptionChannel(cha);
						Cipher decryptCipher = cha.decryptCipher(secretKey,
								ivParArr);
						try {
							byte[] sealedBytes = (byte[]) sealed
									.getObject(decryptCipher);
							byte[] sealedDecode64 = Base64.decode(sealedBytes);
							Object builderObject = bytesToObject(sealedDecode64);
							Request builderRequest = (Request) builderObject;

							if (builderRequest instanceof LoginRequest) {
								returnResponse = login((LoginRequest) builderRequest);
							} else if (builderRequest instanceof CreditsRequest) {
								returnResponse = (CreditsResponse) credits();
							} else if (builderRequest instanceof BuyRequest) {
								returnResponse = buy((BuyRequest) builderRequest);
							} else if (builderRequest instanceof ListRequest) {
								returnResponse = list();
							} else if (builderRequest instanceof DownloadTicketRequest) {
								returnResponse = download((DownloadTicketRequest) builderRequest);
							} else if (builderRequest instanceof UploadRequest) {
								returnResponse = upload((UploadRequest) builderRequest);
							} else if (builderRequest instanceof LogoutRequest) {
								isLogoutRequest = true;
								returnResponse = logout();
							}
							builderRequest = null;
							builderObject = null;
						} catch (IllegalBlockSizeException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (BadPaddingException e) {
							e.printStackTrace();
						}

					}
				} else {
					requestNoSecretKey = new MessageResponse("Unknown object");
				}

				if (returnResponse != null) {
					byte[] requestToBytes = objectToBytes(returnResponse);

					if (requestToBytes == null) {
						System.out.println("Request login is null");
					}

					byte[] request64 = Base64.encode(requestToBytes);

					Cipher cipher = encryptChannel.encryptCipher(secretKey,
							ivParArr);
					SealedObject requestSealed = null;
					System.out.println("Set up sealed object");
					System.out.println("object class is: " + returnResponse.getClass());
					
					try {
						requestSealed = new SealedObject(request64, cipher);
					} catch (IllegalBlockSizeException e) {
						e.printStackTrace();
					}
					out.writeObject(requestSealed);
					out.flush();
					returnResponse = null;
					if(isLogoutRequest){
						System.out.println("Making current user null");
						currentUser = "";
					}
				}

				if (resp != null) {
					System.out.println("req was not null");
					System.out.println("response is " + resp.getClass());
					out.writeObject(resp);
					out.flush();
					resp = null;

				}
				if (requestNoSecretKey != null) {
					System.out.println("request no secret key is: "
							+ requestNoSecretKey);
					out.writeObject(requestNoSecretKey);
					out.flush();
					requestNoSecretKey = null;
				}

			} catch (IOException e) {
			} catch (NullPointerException e) {
			}
		}
	}
	public boolean authenticateFinalMessage(AesChannelRequest request){
		// fianal message should just be the proxy challenge
		byte[] recievedProxyChallenge = Base64.decode(request.getMessage());
		
		TcpChannel cha = new SimpleTcpChannel();
		cha = new AesEncryptionChannel(cha);
		Cipher decryptCipher = cha.decryptCipher(secretKey,
				ivParArr);
		
		try {
			byte[]decrypted = decryptCipher.doFinal(recievedProxyChallenge);
			byte[]actualProxyChall = Base64.decode(proxyChallenge);
			
			boolean verify = MessageDigest.isEqual(decrypted, actualProxyChall);
			return verify;
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	private byte[]proxyChallenge;
	// 2nd message
	public ProxyAuthenticationResponse authenticateRSA(ChannelRequest request) {
		byte[] fromClient = request.getMessage();
		TcpChannel proxyChannel = new SimpleTcpChannel();
		proxyChannel = new ProxyResponseChannel(proxyChannel);

		// process the 2nd message and send back to the user
		byte[] proxResponse = proxyChannel.proxyReturnMessage(fromClient,
				userList);

		// error checking for no keys
		String proxResponseString = new String(proxResponse);
		if (proxResponseString.equals(noKeyFound)) {
			return new ProxyAuthenticationResponse(proxResponse);
		}

		String userName = proxyChannel.returnUser();
		this.proxyChallenge = proxyChannel.returnProxyChallenge();
		System.out.println("Test that the username is correct: " + userName);
		this.currentUser = userName;

		// create the global variables
		getSecretKeyAndIVPar(userName);

		System.out.println("Proxy Response arr : " + proxResponse);

		ProxyAuthenticationResponse proxyAuthResponse = new ProxyAuthenticationResponse(
				proxResponse);
		System.out.println("Writing back the response");
		return proxyAuthResponse;
	}

	// setting up aes channel

	public void getSecretKeyAndIVPar(String username) {
		User userModel = userList.get(currentUser);
		secretKey = userModel.getSecretKey();
		ivParArr = userModel.getIvPar();
	}
	
	@Override
	@Command
	public LoginResponse login(LoginRequest request) throws IOException {

		String newUsername = request.getUsername();
		String newPassword = request.getPassword();

		if (userList.containsKey(newUsername)) {
			System.out.println("userlist contains " + newUsername);
			if (userList.get(newUsername).getPassword().equals(newPassword)) {
				userList.get(newUsername).setOnline(true);
				currentUser = newUsername;
				System.out.println("returning success");
				return new LoginResponse(Type.SUCCESS);
			}
		}
		return new LoginResponse(Type.WRONG_CREDENTIALS);
	}

	@Override
	@Command
	public Response credits(){

		if (currentUser != null) {
			Long credits = userList.get(currentUser).getCredits();
			return new CreditsResponse(credits);
		}
		return new MessageResponse("You are not logged in!");
	}

	@Override
	@Command
	public Response buy(BuyRequest credits) throws IOException {
		if (currentUser != null) {
			synchronized (userList) {
				long curr = userList.get(currentUser).getCredits();
				curr += credits.getCredits();
				userList.get(currentUser).setCredits(curr);
				return new BuyResponse(curr);
			}
		}
		return new MessageResponse("You are not logged in!");
	}

	@Override
	@Command
	public Response list() throws IOException {

		Set<String> filenames = new HashSet<String>();
		if (files.isEmpty()) {
			return new ListResponse(filenames);
		}
		for (FileInfo f : files) {
			filenames.add(f.getName());
		}

		return new ListResponse(filenames);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Command
	public Response download(DownloadTicketRequest request) throws IOException {
		boolean fileExists = false;
		FileInfo dlFile = null;
		for (FileInfo f : files) {
			if (f.getName().equals(request.getFilename())) {
				fileExists = true;
				dlFile = f;
				break;
			}
		}
		if (!fileExists) {
			return new MessageResponse("!download file doesn't exist");
		}
		if (fileServers.isEmpty()) {
			return new MessageResponse("!download no download server available");
		}
		
		TcpChannel chan = new SimpleTcpChannel();
		chan = new HmacChannel(chan);
		
		FileServer leastUsed = null;
		long usage;
		Socket tcpSocket;
		ObjectInputStream fsIn;
		ObjectOutputStream fsOut;
		boolean searchServer = true;
		ArrayList<FileServer> noFile = new ArrayList<FileServer>();

		while (searchServer) {
			usage = Long.MAX_VALUE;
			for (FileServer f : fileServers) {
				if (f.getUsage() < usage && f.isOnline() && !noFile.contains(f)) {
					usage = f.getUsage();
					leastUsed = f;
				}
			}
			tcpSocket = new Socket(leastUsed.getAddress(), leastUsed.getPort());
			fsIn = new ObjectInputStream(tcpSocket.getInputStream());
			fsOut = new ObjectOutputStream(tcpSocket.getOutputStream());
			
			
			
			VersionRequest versionReq = new VersionRequest(request.getFilename());
			String serializedString = objectToString(versionReq);
			String hmacKey = returnSecretKey();
			String generatedHmac = chan.generateHmac(hmacKey, serializedString);
			List<Object>toServer = new ArrayList<Object>();
			toServer.add(generatedHmac);
			toServer.add(versionReq);
			fsOut.writeObject(toServer);
			fsOut.flush();
			int version = -1;
			long size = -1;
			VersionResponse response = null;
			try {
				Object input = fsIn.readObject();
				if(input instanceof List<?>){
					List<Object>inputList = new ArrayList<Object>();
					inputList.addAll((Collection<? extends String>) input);
					if(inputList.size()==2){
						String recievedContents = objectToString(inputList.get(1));
						boolean verify = chan.verifyHmac(hmacKey, inputList.get(0).toString(), recievedContents); // 0 is hashmac 1 is contents
						if(verify){
							response = (VersionResponse)inputList.get(1);
						}
					}
				}
				
				version = response.getVersion();
				System.out.println("Version: " + version);
			} catch (ClassNotFoundException e) {

			}
			fsIn.close();
			fsOut.close();
			tcpSocket.close();
			if (version == dlFile.getVersion()) {
				tcpSocket = new Socket(leastUsed.getAddress(),
						leastUsed.getPort());
				fsIn = new ObjectInputStream(tcpSocket.getInputStream());
				fsOut = new ObjectOutputStream(tcpSocket.getOutputStream());
				InfoRequest infoReq = new InfoRequest(request.getFilename());
				String gneratedHMAC = chan.generateHmac(hmacKey, objectToString(infoReq));
				List<Object>toServer2 = new ArrayList<Object>();
				toServer2.add(gneratedHMAC);
				toServer2.add(infoReq);
				fsOut.writeObject(toServer2);
				InfoResponse infoResponse = null;
				try {
					Object input = fsIn.readObject();
					if(input instanceof List<?>){
						List<Object>inputList = new ArrayList<Object>();
						inputList.addAll((Collection<? extends String>) input);
						if(inputList.size()==2){
							String recievedContents = objectToString(inputList.get(1));
							boolean verify = chan.verifyHmac(hmacKey, inputList.get(0).toString(), recievedContents); // 0 is hashmac 1 is contents
							if(verify){
								infoResponse = (InfoResponse)inputList.get(1);
							}
						}
					}
					size = infoResponse.getSize();
				} catch (ClassNotFoundException e) {

				}
				fsIn.close();
				fsOut.close();
				tcpSocket.close();

				if (size > -1) {
					for (FileServer f : fileServers) {
						if (f.getAddress().equals(leastUsed.getAddress())
								&& f.getPort() == leastUsed.getPort()) {
							synchronized (fileServers) {
								long newUsage = f.getUsage() + size;
								f.setUsage(newUsage);
							}
						}
					}
					searchServer = false;
					if (userList.get(currentUser).getCredits() < size) {
						return new MessageResponse(
								"!download not enough credits");
					} else {
						// if the file is in the file list then increment else
						// add to downloaded file list
						if (downloadedFiles.returnDownloadedFiles()
								.containsKey(dlFile.getName())) {
							downloadedFiles.incrementFile(dlFile.getName());
							System.out.println("Test (Increment download: "
									+ downloadedFiles.returnDownloadedFiles()
											.get(dlFile.getName()));
						} else {
							downloadedFiles.addFile(dlFile.getName());
							System.out.println("Test (Add download: "
									+ downloadedFiles.returnDownloadedFiles()
											.get(dlFile.getName()));
						}

						String checksum = ChecksumUtils.generateChecksum(
								currentUser, dlFile.getName(), version, size);
						synchronized (userList) {
							long credits = userList.get(currentUser)
									.getCredits() - size;
							userList.get(currentUser).setCredits(credits);

							return new DownloadTicketResponse(
									new DownloadTicket(currentUser,
											dlFile.getName(), checksum,
											leastUsed.getAddress(),
											leastUsed.getPort()));

						}
					}
				} else {
					noFile.add(leastUsed);
				}
			} else {
				noFile.add(leastUsed);
			}
			if (noFile.containsAll(fileServers)) {
				searchServer = false;
			}
		}

		return new MessageResponse("!download file not available");
	}

	@SuppressWarnings("unchecked")
	@Override
	@Command
	public MessageResponse upload(UploadRequest request) throws IOException {
		Socket tcpSocket;
		ObjectInputStream fsIn;
		ObjectOutputStream fsOut;
		int uploadcount = 0;
		boolean fileExists = false;
		long filesize = 0;
		UploadRequest newrequest = null;
		for (FileInfo f : files) {
			synchronized (files) {
				if (f.getName().equals(request.getFilename())) {
					newrequest = new UploadRequest(request.getFilename(),
							f.getVersion() + 1, request.getContent());
					fileExists = true;
					break;
				}
			}
		}
		if (!fileExists) {
			newrequest = new UploadRequest(request.getFilename(), 0,
					request.getContent());
		}

		for (FileServer f : fileServers) {
			if (f.isOnline()) {
				tcpSocket = new Socket(f.getAddress(), f.getPort());
				fsOut = new ObjectOutputStream(tcpSocket.getOutputStream());
				fsIn = new ObjectInputStream(tcpSocket.getInputStream());
				TcpChannel chan = new SimpleTcpChannel();
				chan = new HmacChannel(chan);
				String hmacKey = returnSecretKey();
				String gneratedHMAC = chan.generateHmac(hmacKey, objectToString(newrequest));
				List<Object>toServer2 = new ArrayList<Object>();
				toServer2.add(gneratedHMAC);
				toServer2.add(newrequest);
				fsOut.writeObject(toServer2);		
				fsOut.flush();
				try {
					MessageResponse resp = null;
					Object input = fsIn.readObject();
					if(input instanceof List<?>){
						List<Object>inputList = new ArrayList<Object>();
						inputList.addAll((Collection<? extends Object>) input);
						if(inputList.size()==2){
							String recievedContents = objectToString(inputList.get(1));
							boolean verify = chan.verifyHmac(hmacKey, inputList.get(0).toString(), recievedContents); // 0 is hashmac 1 is contents
							if(verify){
								resp = (MessageResponse)inputList.get(1);
							}
						}
					}
					if (resp.getMessage().contains("!upload successful")) {
						uploadcount++;
						filesize = Long
								.parseLong(resp.getMessage().split(" ")[2]);
					}
				} catch (ClassNotFoundException e) {

				}
				fsIn.close();
				fsOut.close();
				tcpSocket.close();
			}
		}
		if (uploadcount > 0) {
			if (!fileExists) {
				files.add(new FileInfo(request.getFilename(), 0));
			} else {
				for (FileInfo f : files) {
					if (f.getName().equals(request.getFilename())) {
						f.setVersion(f.getVersion() + 1);
						break;
					}
				}
			}

			long credits = userList.get(currentUser).getCredits() + filesize
					* 2;
			userList.get(currentUser).setCredits(credits);
			return new MessageResponse("!upload successful");
		}
		return new MessageResponse("!upload failed");
	}

	@Override
	@Command
	public MessageResponse logout() throws IOException {
		System.out.println("Trying logout in proxy");
		if (currentUser != null) {
			userList.get(currentUser).setOnline(false);
			//currentUser = null;
			System.out.println("The log out should be success");
			return new MessageResponse("!logout successful");
		}
		System.out.println("The log out should be fail");
		return new MessageResponse("!logout you are not logged in!");
	}

	public synchronized Object bytesToObject(byte[] message) {

		try {
			ByteArrayInputStream inStream = new ByteArrayInputStream(message);
			ObjectInputStream is = new ObjectInputStream(inStream);
			Object newObject = is.readObject();

			System.out.println("Test the object: " + newObject);
			try {
				inStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}
			try {
				//if (is != null) {
					is.close();
				//}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out
						.println("Ignore exception bytes to object Aes encryption channel");
			}

			System.out.println("Returning object conversion");
			return newObject;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;

	}
	public synchronized byte[] objectToBytes(Object message) {
		try {
			ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
			ObjectOutput objectOut = new ObjectOutputStream(byteArrayOs);
			objectOut.writeObject(message);
			byte[] sealedBytes = byteArrayOs.toByteArray();
			objectOut.close();
			byteArrayOs.close();

			return sealedBytes;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	public synchronized String objectToString(Object object){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream objectOs = null;
		try {
			objectOs = new ObjectOutputStream(baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.writeObject(object);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toString();

	}
	public String returnSecretKey(){
		File file = new File(hmacDirectory);
		if(!file.exists()){
			System.out.println("No existing hmac file");
			return "No hmac file";
		}
		StringBuilder hmacFile = new StringBuilder();
		Scanner scan;
		try {
			scan = new Scanner(file);
			while(scan.hasNextLine()){
				hmacFile.append(scan.nextLine());
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String keyContents = hmacFile.toString();
		return keyContents;
	}
}
