package proxy;


import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import cli.Command;
import cli.Shell;
import model.FileServer;
import model.FileServerInfo;
import model.User;
import model.UserInfo;
import message.Response;
import message.response.FileServerInfoResponse;
import message.response.MessageResponse;
import message.response.UserInfoResponse;

public class ProxyCli implements IProxyCli {
	
    private ConcurrentHashMap<String,User> userList = new ConcurrentHashMap<String,User>();
    private ConcurrentLinkedQueue<FileServer> fileServers = new ConcurrentLinkedQueue<FileServer>();
    private ProxyConnectListener connectListener;
    private IsAliveListener isAliveListener;
    private Shell shell;
    
	
	public ProxyCli (ProxyConnectListener connectListener, IsAliveListener isAliveListener, ConcurrentHashMap<String,User> userList, ConcurrentLinkedQueue<FileServer> fileServers, Shell shell) {
		this.connectListener = connectListener;
		this.isAliveListener = isAliveListener;
		this.userList = userList;
		this.fileServers = fileServers;
		this.shell = shell;
	}
	
	@Override
	@Command
	public Response fileservers() {
		ArrayList<FileServerInfo> servers = new ArrayList<FileServerInfo>();
		for(FileServer f : fileServers) {
			servers.add(new FileServerInfo(f.getAddress(),f.getPort(),f.getUsage(),f.isOnline()));
		}
		return new FileServerInfoResponse(servers);
	}

	@Override
	@Command
	public Response users() {
		ArrayList<UserInfo> users = new ArrayList<UserInfo>();
		for(User u : userList.values()) {
			users.add(new UserInfo(u.getName(),u.getCredits(),u.isOnline()));
		}
		return new UserInfoResponse(users);
	}

	@Override
	@Command("!exit")
	public MessageResponse exit() {
		isAliveListener.shutdown();
		connectListener.shutdown();
        for(User u : userList.values()) {
                u.setOnline(false);
        }
        try {
			System.in.close();
		} catch (IOException e) {
		}
        shell.close();
        return new MessageResponse("Shutting down proxy now");
	}

}
