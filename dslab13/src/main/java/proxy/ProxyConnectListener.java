package proxy;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import model.DownloadedFiles;
import model.FileInfo;
import model.FileServer;
import model.User;

public class ProxyConnectListener implements Runnable {
	
	private Socket clientSocket;
	private ServerSocket serverSocket;
	private ArrayList<Socket> sockets = new ArrayList<Socket>();
    private ConcurrentHashMap<String,User> userList;
    private ConcurrentLinkedQueue<FileServer> fileServers;
    private ConcurrentLinkedQueue<FileInfo> files;
    private ExecutorService threadPool =  Executors.newCachedThreadPool();
    private boolean accept = true;
	private DownloadedFiles downloadedFiles;
	private String hmacDirectory;
    
	public ProxyConnectListener(ServerSocket socket, ConcurrentHashMap<String,User> userList, 
			ConcurrentLinkedQueue<FileServer> fileServers, ConcurrentLinkedQueue<FileInfo> files, DownloadedFiles downloadedFiles, String hmacDirectory) {
		this.serverSocket = socket;
		this.userList = userList;
		this.fileServers = fileServers;
		this.files = files;
		this.downloadedFiles = downloadedFiles;
		this.hmacDirectory = hmacDirectory;
	}
    @Override
	public void run()  {
		
		while(accept) {
            try {
                clientSocket = serverSocket.accept();
                sockets.add(clientSocket);
                Proxy proxy = new Proxy(userList,clientSocket,fileServers,files, downloadedFiles,hmacDirectory);
                
                threadPool.execute(proxy);
            } catch (IOException e) {
                System.err.println("Accept failed.");
            }           
            
        }
	}
	
	public void shutdown() {
		accept = false;
		for(Socket s : sockets) {
			try {
				s.shutdownInput();
				s.shutdownOutput();
				s.close();
			} catch (IOException e) {
			}	
		}

		try {
			serverSocket.close();
		} catch (IOException e) {

		}
		
		threadPool.shutdown();
	}
	
}
