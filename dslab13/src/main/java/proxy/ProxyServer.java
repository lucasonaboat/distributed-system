package proxy;

import util.ComponentFactory;
import util.Config;
import cli.Shell;

public class ProxyServer {
	
	
	public static void main(String[] args) {
		Config config = new Config("proxy");
		Shell shell = new Shell("proxy", System.out, System.in);
		
		ComponentFactory comp = new ComponentFactory();
		try 
		{
			comp.startProxy(config, shell);
		} 
		catch (Exception e) {
		//e.printStackTrace();
		}
		
		
	}
	

}
