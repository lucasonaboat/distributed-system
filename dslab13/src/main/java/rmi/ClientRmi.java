package rmi;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientRmi {
	private ClientRmi(){
		
	}
	public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
        	
        	//System.out.println(Naming.list("rmi://localhost:1099/"));;
           // Registry registry = LocateRegistry.getRegistry(host);
            String rmiName = "rmi://localhost:1099/FileServerRmi";
            System.out.println("Client looking up " + rmiName);
            
        	FileServerRmi stub = (FileServerRmi)Naming.lookup(rmiName);
            
            Integer response = stub.getReadQuorum();
            System.out.println("Get read quorum: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
	
}
