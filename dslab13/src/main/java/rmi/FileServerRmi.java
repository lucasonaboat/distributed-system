package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.concurrent.ConcurrentMap;

public interface FileServerRmi extends Remote {
	public Integer getReadQuorum() throws RemoteException;

	public Integer getWriteQuorum() throws RemoteException;

	public ConcurrentMap<String, Integer> getTopThreeDownloads()
			throws RemoteException;

	public String subscribe(String filename) throws RemoteException;

	public void setPublicKey(String publicKey) throws RemoteException;

	public void getPublicKey() throws RemoteException;

}
