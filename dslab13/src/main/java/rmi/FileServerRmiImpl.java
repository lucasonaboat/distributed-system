package rmi;


import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class FileServerRmiImpl  extends java.rmi.server.UnicastRemoteObject implements FileServerRmi{
	protected FileServerRmiImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private static final long serialVersionUID = -3665904139368394228L;

	public static void main(String args[]) {
        
        try {
        	//Config config = new Config("mc");
        	//String bindingName = config.getString("binding.name");
        	//int remotePort = config.getInt("proxy.rmi.port");
        	
        	FileServerRmi obj = new FileServerRmiImpl();
        	FileServerRmi stub = (FileServerRmi)UnicastRemoteObject.exportObject(obj, 0);
        	
        	
            // Bind the remote object's stub in the registry
            //Registry registry = LocateRegistry.getRegistry();
            String registryName = "FileServerRmi";
            System.out.println("Registering Server name as: " + registryName);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind("FileServerRmi", stub);
            
            
            //registry.bind(registryName, stub); ///bindingName

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
	

	public Integer getReadQuorum() throws RemoteException {
		// TODO Auto-generated method stub
		return 2;
	}

	
	public Integer getWriteQuorum() throws RemoteException {
		// TODO Auto-generated method stub
		return 3;
	}

	
	public ConcurrentMap<String, Integer> getTopThreeDownloads()
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String subscribe(String filename) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setPublicKey(String publicKey) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	
	public void getPublicKey() throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
}
