package rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;



public class FileServerRmiServer {
	public FileServerRmiServer(){
		try {
			//Config config = new Config("mc");
        	//String bindingName = config.getString("binding.name");
			FileServerRmi fileServer = new FileServerRmiImpl();
			Naming.rebind("rmi://localhost:1099/FileServerRmi", fileServer);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String args[]){
		new FileServerRmiServer();
	}
}
