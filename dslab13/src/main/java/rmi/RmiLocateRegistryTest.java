package rmi;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RmiLocateRegistryTest {

	private final static String NEW_LINE = System.getProperty("line.separator");
	public static void main(String[] args) {
		
		String host = "localhost";
		int port = 1099;
		
		try
	      {
			
	         final Registry registry = LocateRegistry.getRegistry();
	         final String[] boundNames = registry.list();
	         System.out.println(
	            "Names bound to RMI registry at host " + host + " and port " + port + ":");
	         for (final String name : boundNames)
	         {
	            System.out.println("\t" + name);
	         }
	      }
	      catch (ConnectException connectEx)
	      {
	         System.err.println(
	              "ConnectionException - Are you certain an RMI registry is available at port "
	            + port + "?" + NEW_LINE + connectEx.toString());
	      }
	      catch (RemoteException remoteEx)
	      {
	         System.err.println("RemoteException encountered: " + remoteEx.toString());
	      }
	}

}
