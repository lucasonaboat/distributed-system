package server;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bouncycastle.util.encoders.Base64;

import cli.Command;
import util.ChecksumUtils;
import message.Request;
import message.Response;
import message.request.DownloadFileRequest;
import message.request.InfoRequest;
import message.request.ListRequest;
import message.request.UploadRequest;
import message.request.VersionRequest;
import message.response.DownloadFileResponse;
import message.response.InfoResponse;
import message.response.ListResponse;
import message.response.MessageResponse;
import message.response.VersionResponse;
import model.FileInfo;
import model.channel.HmacChannel;
import model.channel.SimpleTcpChannel;
import model.channel.TcpChannel;

public class FileServer implements IFileServer, Runnable {
	
	private ConcurrentLinkedQueue<FileInfo> files;
	private Socket socket;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private String directory;
	private String hmacDirectory;
	public FileServer(Socket socket, String directory, ConcurrentLinkedQueue<FileInfo> files, String hmacDirectory) {
		this.socket = socket;
		this.directory = directory;
		this.files = files;
		this.hmacDirectory = hmacDirectory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			
		} catch (IOException e) {
			System.err.println("Error creating In-/Outputstream");
		}
		Object input = null;
		
		try {
			input = in.readObject();
			System.out.println("Input at server is: " + input);
			String recievedHmacString = "";
			Object msg;
			
			// if the user is contacting the fileserver, no HMAC needed.
			if(input instanceof DownloadFileRequest){
				System.out.println("Is instnace of download request");
				out.writeObject(download((DownloadFileRequest)input));
			}
			// if the input is a list then it means there is an HMAC (request from proxy)
			if(input instanceof List<?>){
				System.out.println("Is arraylist");
				List<Object>inputList = new ArrayList<Object>();
				inputList.addAll((Collection<? extends String>) input);
				if(inputList.size()==2){
					System.out.println("size equal to two");
					recievedHmacString = inputList.get(0).toString();
					System.out.println("hmac received: " + recievedHmacString);
					msg = inputList.get(1);
					String msgContents = objectToString(msg);
					System.out.println("received msg: " + msg);
					
					File file = new File(hmacDirectory);
					if(!file.exists()){
						System.out.println("No existing hmac file");
					}
					
					StringBuilder hmacFile = new StringBuilder();
					Scanner scan = new Scanner(file);
					while(scan.hasNextLine()){
						hmacFile.append(scan.nextLine());
					}
					String keyContents = hmacFile.toString();
					
					TcpChannel chan = new SimpleTcpChannel();
					chan = new HmacChannel(chan);
					
					System.out.println("key contents: " + keyContents);
					boolean verify = chan.verifyHmac(keyContents, recievedHmacString, msgContents);
					
					Response returnObject = null;
					
					if(verify){
						System.out.println("Verify was true");
						Request inputReq = (Request)msg;
						
						if(inputReq instanceof ListRequest){
							System.out.println("Is instnace of list request");
							returnObject = list();
							//out.writeObject(list());
						}else if(inputReq instanceof InfoRequest){
							System.out.println("Is instnace of info request");
							returnObject = info((InfoRequest)inputReq);
						}else if(inputReq instanceof VersionRequest){
							returnObject = version((VersionRequest)inputReq);
						}else if(inputReq instanceof UploadRequest){
							System.out.println("Is instnace of upload request");
							returnObject = upload((UploadRequest)inputReq);
						}
					}
					else{
						System.out.println("Verify was false");
						returnObject = new MessageResponse("HMAC not verified");
						//out.writeObject(new MessageResponse("HMAC not verified"));
					}
					String objectString = objectToString(returnObject);
					String generatedHmac = chan.generateHmac(keyContents, objectString);
					List<Object>returnList = new ArrayList<Object>();
					returnList.add(generatedHmac);
					returnList.add(returnObject);
					out.writeObject(returnList);
					out.flush();
				}
				
				
			}
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
			in.close();
			out.close();
			
		} catch (ClassNotFoundException e) {
			
		} catch (IOException e) {
			System.err.println("Failed to read input");
		}
		catch (NullPointerException e) {			
		}
//		try {
//			if(input.getClass().equals(ListRequest.class)) {					
//				out.writeObject(list());				
//			}
//			else if(input.getClass().equals(DownloadFileRequest.class)) {
//				out.writeObject(download((DownloadFileRequest)input));
//			}
//			else if(input.getClass().equals(InfoRequest.class)) {
//				out.writeObject(info((InfoRequest)input));
//			}
//			else if(input.getClass().equals(VersionRequest.class)) {
//				out.writeObject(version((VersionRequest)input));
//			}
//			else if(input.getClass().equals(UploadRequest.class)) {
//				out.writeObject(upload((UploadRequest)input));
//			}
//			else if(input.getClass().equals(String.class)){
//				System.out.println("�s a stringggg");
//			}
//			
//			socket.shutdownInput();
//			socket.shutdownOutput();
//			socket.close();
//			in.close();
//			out.close();
//			
//		} catch (IOException e) {
//			System.err.println("Failed to send message");
//		}
//		catch (NullPointerException e) {			
//		}
			
		
	}

	@Override
	@Command
	public Response list() throws IOException {
		
		Set<String> filenames = new HashSet<String>();
		if(files.isEmpty()) {
			return new ListResponse(filenames);
		}
		
		for(FileInfo f : files) {
			filenames.add(f.getName());
		}
		
		return new ListResponse(filenames);
	}

	@Override
	@Command
	public Response download(DownloadFileRequest request) throws IOException {
		String filePath = directory + "/" + request.getTicket().getFilename();
		File file = new File(filePath);
		
		if(file.exists()) {
			int version = 0;
			for(FileInfo f : files) {
				if(f.getName().equals(request.getTicket().getFilename())) {
					version = f.getVersion();
					break;
				}
			}
			if(ChecksumUtils.verifyChecksum(request.getTicket().getUsername(), file, version, request.getTicket().getChecksum())) {
				String content = "";
				FileReader fr = new FileReader(file);
			    BufferedReader br = new BufferedReader(fr);
			    char [] contentChars = new char[(int)file.length()];
			    
			    
			    br.read(contentChars);
			    
			    br.close();
			    fr.close();
			    content = new String(contentChars);
			    byte [] byteContent = content.getBytes();
				return new DownloadFileResponse(request.getTicket(), byteContent);
			}
			return new MessageResponse("!download invalid checksum");
		}
		return new MessageResponse("!download file not available");
	}

	@Override
	@Command
	public Response info(InfoRequest request) throws IOException {
		String filePath = directory + "/" + request.getFilename();
		File file = new File(filePath);
		if(file.exists()) {
			return new InfoResponse(request.getFilename(), file.length());
		}
		return new InfoResponse(request.getFilename(), -1);
	}

	@Override
	@Command
	public Response version(VersionRequest request) throws IOException {
		for(FileInfo f : files) {
			if(f.getName().equals(request.getFilename())) {
				VersionResponse versRes = new VersionResponse(f.getName(), f.getVersion());
				System.out.println("Returning version response: " + versRes);
				return versRes;
				//return new VersionResponse(f.getName(),f.getVersion());
			}
		}
		return new VersionResponse(request.getFilename(),-1);
	}

	@Override
	@Command
	public MessageResponse upload(UploadRequest request) throws IOException {	
		FileWriter fw = new FileWriter(directory + "/" + request.getFilename(),false);
		fw.write(new String(request.getContent()));
		fw.close();
		boolean exists = false;
		synchronized(files) {
			for(FileInfo f : files) {
				if(f.getName().equals(request.getFilename())) {
					f.setVersion(request.getVersion());
					exists = true;
					break;
				}
			}
			if(!exists) {
				//System.out.println("UPLOAD");
				files.add(new FileInfo(request.getFilename(), request.getVersion()));
			}
		}
		File up = new File(directory + "/" + request.getFilename());
		return new MessageResponse("!upload successful " + up.length());
	}
	
	public synchronized Request stringToObject(String message) {

		try {
			System.out.println("Original string: " + message);
			byte[]msgBytes = message.getBytes();
			
			ByteArrayInputStream inStream = new ByteArrayInputStream(msgBytes);
			ObjectInputStream is = new ObjectInputStream(inStream);
			Request newObject = (Request)is.readObject();
			System.out.println("Test the object: " + newObject);
			System.out.println("Returning object conversion");
			return newObject;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Returning null");
		return null;

	}
	
	public synchronized String objectToString(Object object){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream objectOs = null;
		try {
			objectOs = new ObjectOutputStream(baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.writeObject(object);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectOs.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toString();
		
				
		
	}
}


