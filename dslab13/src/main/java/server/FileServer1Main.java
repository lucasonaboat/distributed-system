package server;

import cli.Shell;
import util.ComponentFactory;
import util.Config;

public class FileServer1Main {
	
	public static void main(String[] args) {
		Config config = new Config("fs1");
		Shell shell = new Shell("fs1", System.out, System.in);
		
		ComponentFactory comp = new ComponentFactory();
		try 
		{
			comp.startFileServer(config, shell);
		} 
		catch (Exception e) {
		}
		
		
	}
	

}
