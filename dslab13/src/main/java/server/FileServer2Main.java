package server;

import cli.Shell;
import util.ComponentFactory;
import util.Config;

public class FileServer2Main {
	
	public static void main(String[] args) {
		Config config = new Config("fs2");
		Shell shell = new Shell("fs2", System.out, System.in);
		
		ComponentFactory comp = new ComponentFactory();
		try 
		{
			comp.startFileServer(config, shell);
		} 
		catch (Exception e) {
		}
		
		
	}
	

}
