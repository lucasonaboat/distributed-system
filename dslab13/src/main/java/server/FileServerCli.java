package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import cli.Command;
import cli.Shell;
import message.response.MessageResponse;
import model.FileInfo;

public class FileServerCli implements IFileServerCli {
	
	private String host;
	private int port;
	private int proxyUdpPort;
	private int alivePeriod;
	private DatagramSocket udpSocket;
	private FileServerConnectListener connectListener;
	private Timer aliveSignal;
	private Shell shell;
	
	public FileServerCli(String host,int port, int proxyUdpPort, FileServerConnectListener connectListener, int alivePeriod, DatagramSocket udpSocket, Shell shell) {
		this.host = host;
		this.port = port;
		this.proxyUdpPort = proxyUdpPort;
		this.alivePeriod = alivePeriod;
		this.connectListener = connectListener;
		this.udpSocket = udpSocket;
        this.shell = shell;
		
		TimerTask action = new TimerTask() {
            @Override
			public void run() { sendAliveSignal(); }
        };
        
        aliveSignal = new Timer();
        aliveSignal.schedule(action, 3000, alivePeriod);
	}
	

	@Override
	@Command
	public MessageResponse exit() {
		connectListener.shutdown();
		aliveSignal.cancel();
		aliveSignal.purge();
		udpSocket.close();		
		try {
			System.in.close();
		} catch (IOException e) {
		}
		shell.close();
		return new MessageResponse("Shutting down file server now");
	}
	
	public void sendAliveSignal() {
		String message = null;
		message = "!alive " + port;
		DatagramPacket sendPacket;	
		try {
			sendPacket = new DatagramPacket(message.getBytes(),message.getBytes().length, InetAddress.getByName(host), proxyUdpPort);
			udpSocket.send(sendPacket);
		} catch (IOException e) {
			System.err.println("Failed to send isAlive packet");
		}
		
	}

}
