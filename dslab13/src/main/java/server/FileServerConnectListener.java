package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import model.FileInfo;


public class FileServerConnectListener implements Runnable {

	private Socket clientSocket;
	private ServerSocket serverSocket;
	private ArrayList<Socket> sockets = new ArrayList<Socket>();
	private boolean accept = true;
	private ExecutorService threadPool =  Executors.newCachedThreadPool();
	private ConcurrentLinkedQueue<FileInfo> files;
	private String directory;
	private String hmacDirectory;
	
	public FileServerConnectListener(ServerSocket serverSocket, ConcurrentLinkedQueue<FileInfo> files, String directory, String hmacDirectory) {
		this.serverSocket = serverSocket;
		this.files = files;
		this.directory = directory;
		this.hmacDirectory = hmacDirectory;
	}
	
	@Override
	public void run() {


		while(accept) {
            try {
                clientSocket = serverSocket.accept();
                sockets.add(clientSocket);
                FileServer fs = new FileServer(clientSocket,directory,files, hmacDirectory);
                threadPool.execute(fs);
            } catch (IOException e) {
                System.err.println("fs: Accept failed.");
            }           
            
        }
		
	}
	
	public void shutdown() {
		accept = false;
		for(Socket s : sockets) {
			try {
				s.shutdownInput();
				s.shutdownOutput();
				s.close();
			} catch (IOException e) {
			}	
		}
		try {		
			serverSocket.close();
		} catch (IOException e) {
		}
		
		threadPool.shutdown();
	}

}
