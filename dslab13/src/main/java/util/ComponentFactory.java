package util;

import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import model.DownloadedFiles;
import model.FileInfo;
import model.FileServer;
import model.User;
import cli.Shell;
import client.ClientCli;
import client.IClientCli;
import proxy.IProxyCli;
import proxy.IsAliveListener;
import proxy.ProxyCli;
import proxy.ProxyConnectListener;
import server.FileServerCli;
import server.FileServerConnectListener;
import server.IFileServerCli;

/**
 * Provides methods for starting an arbitrary amount of various components.
 */
public class ComponentFactory {
	/**
	 * Creates and starts a new client instance using the provided {@link Config} and {@link Shell}.
	 *
	 * @param config the configuration containing parameters such as connection info
	 * @param shell  the {@code Shell} used for processing commands
	 * @return the created component after starting it successfully
	 * @throws Exception if an exception occurs
	 */
	public IClientCli startClient(Config config, Shell shell) throws Exception {
		
		Socket tcpSocket = null;
		String proxyHost = config.getString("proxy.host");
		int proxyTcpPort = config.getInt("proxy.tcp.port");
		String directory = config.getString("download.dir");
		
		try {
			tcpSocket = new Socket(proxyHost, proxyTcpPort);
		} catch (UnknownHostException e) {
			System.err.println("Couldn't find host");
            return null;
		} catch (IOException e) {
			System.err.println("client: Connection error");
			return null;
		}
		
		IClientCli client = new ClientCli(tcpSocket, directory, shell);                
		shell.register(client);  
		Thread shellThread = new Thread(shell);

		shellThread.start();
		
		
		return client;
	}

	/**
	 * Creates and starts a new proxy instance using the provided {@link Config} and {@link Shell}.
	 *
	 * @param config the configuration containing parameters such as connection info
	 * @param shell  the {@code Shell} used for processing commands
	 * @return the created component after starting it successfully
	 * @throws Exception if an exception occurs
	 */
	public IProxyCli startProxy(Config config, Shell shell) throws Exception {
		
		System.out.println("Starting proxy");
        int tcpPort = config.getInt("tcp.port");
        int udpPort = config.getInt("udp.port");
        int fsTimeout = config.getInt("fileserver.timeout");
        int fsCheckPeriod = config.getInt("fileserver.checkPeriod");
        
        String hmacDirectory = config.getString("hmac.key");
      
        ServerSocket serverSocket;
        DatagramSocket udpSocket;
	    ConcurrentHashMap<String,User> userList = new ConcurrentHashMap<String,User>();
	    ConcurrentLinkedQueue<FileServer> fileServers = new ConcurrentLinkedQueue<FileServer>();
	    ConcurrentLinkedQueue<FileInfo> files = new ConcurrentLinkedQueue<FileInfo>();
	    ProxyConnectListener connectListener = null;
	    IsAliveListener isAliveListener = null;
	    Thread isAliveThread;
	    Thread connectThread;
		
		
		ResourceBundle bundle = ResourceBundle.getBundle("user");
		Enumeration<String> keys = bundle.getKeys();
		ArrayList<String> users = new ArrayList<String>();
		while(keys.hasMoreElements()) {
            String tmp = keys.nextElement();
            int dot = tmp.indexOf(".");
			String key = tmp.substring(0, dot);
			if(!users.contains(key)) users.add(key);
		}
		
		for(String u : users) {
			long credits = Long.parseLong(bundle.getString(u + ".credits"));
			byte[]empty = " ".getBytes();
			User user = new User(u, credits, false, bundle.getString(u+".password"), empty, empty);
			userList.put(u, user);
		}

		
		try {
			serverSocket = new ServerSocket(tcpPort);
			udpSocket = new DatagramSocket(udpPort);
			DownloadedFiles downloadedFiles = new DownloadedFiles();
			connectListener = new ProxyConnectListener(serverSocket,userList,fileServers,files, downloadedFiles, hmacDirectory);
			connectThread = new Thread(connectListener);
			connectThread.start();
			isAliveListener = new IsAliveListener(udpSocket,fileServers,fsTimeout,fsCheckPeriod,files, hmacDirectory);
			isAliveThread = new Thread(isAliveListener);
			isAliveThread.start();
		} catch (IOException e) {
			System.err.println("proxy: Failed to create socket");
		}
		
		
		IProxyCli proxy = new ProxyCli(connectListener, isAliveListener, userList, fileServers, shell);
		
		shell.register(proxy);
		Thread shellThread = new Thread(shell);

		shellThread.start();
		
		
		return proxy;
	}

	/**
	 * Creates and starts a new file server instance using the provided {@link Config} and {@link Shell}.
	 *
	 * @param config the configuration containing parameters such as connection info
	 * @param shell  the {@code Shell} used for processing commands
	 * @return the created component after starting it successfully
	 * @throws Exception if an exception occurs
	 */
	public IFileServerCli startFileServer(Config config, Shell shell) throws Exception {
		
		ConcurrentLinkedQueue<FileInfo> files = new ConcurrentLinkedQueue<FileInfo>();
		String host = config.getString("proxy.host");
		int port = config.getInt("tcp.port");
		int proxyUdpPort = config.getInt("proxy.udp.port");
		int alivePeriod = config.getInt("fileserver.alive");
		Thread connectThread;
		ServerSocket serverSocket = null;
		DatagramSocket udpSocket = null;
		FileServerConnectListener connectListener = null;
		
		String hmacDirectory = config.getString("hmac.key");
		
		
		String directory = config.getString("fileserver.dir");
		
		File dir = new File(directory);
		if(dir.isDirectory()) {
			for(File f : dir.listFiles()) {
				FileInfo fi = new FileInfo(f.getName(),0);
				files.add(fi);
			}
			
		}
		
		try {
			serverSocket = new ServerSocket(port);
			udpSocket = new DatagramSocket(port);
			connectListener = new FileServerConnectListener(serverSocket,files,directory, hmacDirectory);
			connectThread = new Thread(connectListener);
			connectThread.start();
			
		} catch (IOException e) {
			System.err.println("fs: Error creating socket");
		}		
		
		IFileServerCli fs = new FileServerCli(host, port, proxyUdpPort, connectListener, alivePeriod, udpSocket, shell);
		
		shell.register(fs);
		Thread shellThread = new Thread(shell);

		shellThread.start();
		
		
		return fs;
	}
}
