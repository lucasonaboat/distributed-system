package util;

import java.io.FileReader;
import java.io.IOException;
import java.security.PublicKey;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.User;
import model.channel.Base64Channel;
import model.channel.Channel;
import model.channel.ClientFinalMessageChannel;
import model.channel.ProxyResponseChannel;
import model.channel.SimpleTcpChannel;
import model.channel.TcpChannel;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;

import convert.ConversionService;

public class TestBase64Encoding {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String simpleMessage = "Hello world";

		TcpChannel chan = new SimpleTcpChannel();

		chan = new Base64Channel(chan);

		ConversionService convertor = new ConversionService();
		byte[] c = convertor.convert(simpleMessage, byte[].class);

		System.out.println("Original message: " + simpleMessage);
		System.out.println("testing byte : " + c);

		byte encoded[] = chan.encodeBase64(c);
		System.out.println("Encoded in base64: " + encoded);

		String encodedInString = convertor.convert(encoded, String.class);
		System.out.println("Encoded message in string format: "
				+ encodedInString);

		byte decoded[] = chan.decodeBase64(encoded);
		System.out.println("Decoded from base64: " + decoded);

		String oldMessage = convertor.convert(decoded, String.class);
		System.out.println("Original message: " + oldMessage);

		// or in one step

		String sampleMessage = "All in one step";
		byte[] simpleToBytes = convertor.convert(sampleMessage, byte[].class);

		TcpChannel simple = new SimpleTcpChannel();
		simple = new Base64Channel(simple);

		String result = convertor.convert(
				simple.decodeBase64(simple.encodeBase64(simpleToBytes)),
				String.class);
		System.out.println("Result is: " + result);

		// reading keys
		try {
			System.out.println("Now lets try alice file:");
			String pathToPublicKey = "keys/proxy.pub.pem";
			PEMReader in = new PEMReader(new FileReader(pathToPublicKey));
			PublicKey publicKey = (PublicKey) in.readObject();
			System.out.println(publicKey);
			System.out.println("alice file done!");
			
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String username = "alice";
		
		
		try {
			String pathToPublicKey = "keys/" + username + ".pub.pem".toString();
			PEMReader in1 = new PEMReader(new FileReader(pathToPublicKey));
			PublicKey publicKey1 = (PublicKey) in1.readObject();
			System.out.println(publicKey1);
			in1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// test the login.
		TcpChannel loginTest= new SimpleTcpChannel();
		loginTest = new Channel(loginTest);
		byte[] loginByteArr = loginTest.clientLoginRequest("alice");
		if(loginByteArr ==null){ System.out.println("byte array to login is null");}
		System.out.println("alice login test: " + loginByteArr + " length " + loginByteArr.length);
		
		int totalLength = loginByteArr.length;
		int lengthChallenge = 44;
		int lengthLoginMessage = totalLength - lengthChallenge;
		
		byte[]userLoginByteArr = new byte[lengthLoginMessage];
		byte[]challengeByteArr = new byte[lengthChallenge];
		
		System.arraycopy(loginByteArr, 0, userLoginByteArr, 0, lengthLoginMessage);
		System.arraycopy(loginByteArr, lengthLoginMessage, challengeByteArr, 0, lengthChallenge);
		
		TcpChannel proxyChannel = new SimpleTcpChannel();
		proxyChannel = new ProxyResponseChannel(proxyChannel);
		
		
		
		
		
		TcpChannel base = new SimpleTcpChannel();
		base = new Base64Channel(base);
		
		byte[] loginResultByteArr = base.decodeBase64(userLoginByteArr);
		System.out.println("decoded: " + loginResultByteArr );
		String loginString = convertor.convert(loginResultByteArr, String.class);
		System.out.println("decoded to string " + loginString + " length is : " + loginString.length());
		
		ConcurrentHashMap<String, User> userList= new ConcurrentHashMap<String, User>();
		byte[] empty = " ".getBytes();
		User alice = new User("alice", Long.valueOf(200), true, "12345", empty, empty);
		userList.put("alice", alice);
		byte[]proxResponse = proxyChannel.proxyReturnMessage(loginByteArr, userList);
		if(proxResponse ==null){
			System.out.println("Response is null");
		}
		System.out.println("responsexcxzczxczxc: " + proxResponse.toString());
		
		/*
		
		// seperate into individual array's to test: 
		byte[] decryptedText1 = Base64.decode(proxResponse);
		byte[] decryptedText = Base64.decode(proxResponse);
		
		System.out.println("Total size in test is: " + decryptedText.length + " Array is: " + decryptedText);
		
		// !ok <client-challenge> <proxy-challenge> <secret-key> <iv-parameter>.
		int startingPosition = 35; // to account for the ok message (3bytes) and the client challenge (32bytes) + 1 padding
		int lengthProxyChallenge = 32;
		int lengthSecretKey= 37;
		int lengthIvArr = 16;
		
		byte[] proxyChallenge = new byte[lengthProxyChallenge];
		byte[] secretKey = new byte[lengthSecretKey];
		byte[] ivArr = new byte[lengthIvArr];
		
		System.arraycopy(decryptedText, startingPosition, proxyChallenge, 0, lengthProxyChallenge);
		System.arraycopy(decryptedText, startingPosition + lengthProxyChallenge, secretKey, 0, lengthSecretKey);
		System.arraycopy(decryptedText, startingPosition + lengthProxyChallenge + lengthSecretKey,
				ivArr, 0, lengthIvArr);
		
		System.out.println("Proxy challenge: " + proxyChallenge.toString() + " Lenth : " + proxyChallenge.length);
		System.out.println("Secret key: " + secretKey + " Lenth : " + secretKey.length);
		System.out.println("iv array: " + ivArr + " Lenth : " + ivArr.length);
		
		TcpChannel finalTest = new SimpleTcpChannel();
		finalTest = new ClientFinalMessageChannel(finalTest);
		
		byte[] finalReqFromClient = finalTest.clientSendFinalMessage(proxResponse, "alice");
		System.out.println("From client: " + finalReqFromClient);
		*/
		
		
		
		
//		final String B64 = "a-zA-Z0-9/+";
//		assert loginString.matches("!login \\w+ ["+B64+"]{43}=") : "1st message";
//	    
//		Pattern pattern = Pattern.compile("\\s");
//		Matcher matcher = pattern.matcher(loginString);
//		int positionOfSecondWhiteSpace = 0;
//		int counter = 0;
//		if(matcher.find()){
//			System.out.println("whitespace found!");
//			for(int i=0;i<loginString.length();i++){
//				if(Character.isWhitespace(loginString.charAt(i))){
//					System.out.println("whitespace found at " + i);
//					System.out.println("i minus 1 " + loginString.charAt(i-1));
//					counter++;
//					if(counter ==2){
//						positionOfSecondWhiteSpace = i;
//						
//					}
//				}
//			}
//			
//		}
//		String challenge = loginString.substring(positionOfSecondWhiteSpace);
//		System.out.println("chellenge: " + challenge);
//		
//		System.out.println("challenge length: " + challenge.length());
//		
	    
	    
	}

}
