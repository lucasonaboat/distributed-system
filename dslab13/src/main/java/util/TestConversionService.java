package util;

import convert.ConversionService;

public class TestConversionService {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String catnam = "Barrycat";
		byte[] catname = catnam.getBytes();
		
		System.out.println("Bytes: " + catname);
		String back = new String(catname);
		System.out.println("cat name back: " + back);

		Object cat = catname;
		
		ConversionService con = new ConversionService();
		
		byte[] converted = con.convert(cat, byte[].class);
		
		System.out.println("Con array : " + converted);
		
		String convertedBack = new String(converted);
		
		System.out.println("Con array back : " + convertedBack);
		

	}

}
